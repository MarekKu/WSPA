﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Security
{
    [TestFixture]
    public class Class1
    {
        [Test]
        public void t()
        {
            var h = "09F0C9F0";
            var bytes = StringToByteArray(h);
            var z = Convert.ToBase64String(bytes);


            //var cipherText = "FCC32ADFDE0C5E26A4EB5E155E993E45EC88A1EEF6CD550A1906CB4299EF1C7AE21A86E03D75207154A5EA2F3F3D85D3F49D99902542209A3F551E566265975D30ED8533664300861C4371E02A2549B5B97D53907125CB2225219DB60CD1F71349172CAFD46C9B14500FF36DDD1B5793806F9C49B45215714D2CCC3E91646E8538D852776760F58D8BE415F7A9F261EFDE87FCDF3E44A5087C78C03CDFDF9A06D4A303501113B655D12E44856B493DC9DF3FE667D6CDAE32AE552373556599DA4447F83B";

            var cipherText = "3099FF14474149FFC4750E5DC8ECB3E9510B541464DB99763AE6E86BF031CE10869AB12AD784FCA15733A7DE8334639314A17888E40F29C63735671263EC056EF8BB80A64C58EA584FF44D98A29298E757BD7E12A1301275152C9151AEE3B8C02B555382AF563B24A2720033C884318A7A453E320D320D8EC8D1A1DFAAC457BC4F5A8CFA3BE34E6314B5ED0CCCD772FD3EC2D5855F4805E65547E434E831425CE6E4DCE2414290D5872F9C9A3657CC3490CDC50C1FABEAB2922FEA65934D7F2892A7E3A2";
            var cipherByte = Convert.FromBase64String(cipherText);
           
            var cipherByteSubstring = cipherByte.Take(288).ToArray();
            var keyInfo = "801097B583E74B8583EA6D2627E38CBC08A638AAC8A59C16CCA86D089749A282";
            var key =StringToByteArray(keyInfo);

        //    var unprotectedInput = System.Web.Security.MachineKey.Unprotect(cipherByte, "ApplicationCookie", "v1");




            //var algorithm = new AesManaged();
            //algorithm.Padding = PaddingMode.PKCS7;
            //algorithm.Mode = CipherMode.CBC;
            //algorithm.KeySize = 256;
            //algorithm.BlockSize = 128;
            //var validationAlgorithm = new HMACSHA1();
            //using (SymmetricAlgorithm encryptionAlgorithm = algorithm)
            //{
            //    encryptionAlgorithm.Key = key;
            //    int offset = encryptionAlgorithm.BlockSize / 8; //16
            //    int buffer1Count = validationAlgorithm.HashSize / 8; // 20
            //    int count = checked(cipherByte.Length - offset - buffer1Count); // 16
            //    byte[] numArray = new byte[offset];
            //    Buffer.BlockCopy((Array)cipherByte, 0, (Array)numArray, 0, numArray.Length);
            //    encryptionAlgorithm.IV = numArray; // in HEX: A738E5F98920E37AB14C5F4332D4C7F0
            //    using (MemoryStream memoryStream = new MemoryStream())
            //    {
            //        using (ICryptoTransform decryptor = encryptionAlgorithm.CreateDecryptor())
            //        {
            //            using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, decryptor, CryptoStreamMode.Write))
            //            {
            //                cryptoStream.Write(cipherByte, offset, count);

            //                // Padding is invalid and cannot be removed.
            //                cryptoStream.FlushFinalBlock();
            //                var result = memoryStream.ToArray();
            //            }
            //        }
            //    }
            //}














                var plainBytes = new AesCbcCiphering().DecryptBytesFromBytes(cipherByteSubstring, key);
            var zz = Convert.ToBase64String(plainBytes);
          //  UTF8Encoding specialUtf8Encoding = new UTF8Encoding(false, true);
           // var plainText = specialUtf8Encoding.GetString(plainBytes);
            //decrypt .GlsAuthCookie
            //Validate it
            //return true or false
        }

        public static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }

    }

    public class AesCbcCiphering : AesCiphering
    {
        public override byte[] EncryptBytesToBytes(byte[] originalPayload, byte[] key)
        {
            CheckArguments(originalPayload, key);

            try
            {
                using (var aes = new AesCryptoServiceProvider()
                {
                    Key = key,
                    Mode = CipherMode.CBC,
                    Padding = PaddingMode.PKCS7
                })
                {
                    return EncryptBytesToBytes(originalPayload, aes);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public override byte[] DecryptBytesFromBytes(byte[] encryptedBytes, byte[] key)
        {
            CheckArguments(encryptedBytes, key);

            try
            {
                using (var provider = new AesCryptoServiceProvider())
                {
                    var z = provider.LegalKeySizes;
                    provider.Padding = PaddingMode.Zeros;
                    provider.Key = key;
                    provider.IV = GetIv(encryptedBytes);
                    provider.Mode = CipherMode.CBC;

                    using (var ms = new MemoryStream(GetCipherText(encryptedBytes)))
                    using (var decryptor = provider.CreateDecryptor())
                    using (var cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Read))
                    using (var msW = new MemoryStream())
                    {
                        var buffer = new byte[1024];
                        var read = cs.Read(buffer, 0, buffer.Length);

                        while (read > 0)
                        {
                            msW.Write(buffer, 0, read);
                            read = cs.Read(buffer, 0, buffer.Length);
                        }

                        cs.Flush();

                        var plain = msW.ToArray();
                        var lastByte = plain.Last();
                        plain = plain.Take(plain.Length - lastByte).ToArray();

                        return plain;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private byte[] EncryptBytesToBytes(byte[] input, AesCryptoServiceProvider aes)
        {
            aes.GenerateIV();

            using (var encrypter = aes.CreateEncryptor(aes.Key, aes.IV))
            using (var cipherStream = new MemoryStream())
            {
                using (var tCryptoStream = new CryptoStream(cipherStream, encrypter, CryptoStreamMode.Write))
                using (var tBinaryWriter = new BinaryWriter(tCryptoStream))
                {
                    cipherStream.Write(aes.IV, 0, aes.IV.Length);
                    tBinaryWriter.Write(input);
                    tCryptoStream.FlushFinalBlock();
                }
                return cipherStream.ToArray();
            }
        }

        protected override int IvSize { get; } = 16;
    }

    public abstract class AesCiphering
    {
        public static byte[] GenerateAesKey(int keySize)
        {
            using (AesCryptoServiceProvider myAes = new AesCryptoServiceProvider()
            {
                KeySize = keySize
            })
            {
                return myAes.Key;
            }
        }

        public abstract byte[] EncryptBytesToBytes(byte[] originalPayload, byte[] key);

        public abstract byte[] DecryptBytesFromBytes(byte[] encryptedBytes, byte[] key);

        protected byte[] GetIv(byte[] encryptedBytes)
        {
            var iv = new byte[IvSize];
            Array.Copy(encryptedBytes, 0, iv, 0, IvSize);

            return iv;
        }

        protected virtual byte[] GetCipherText(byte[] encryptedBytes)
        {
            var cipherText = new byte[encryptedBytes.Length - IvSize];
            Array.Copy(encryptedBytes, IvSize, cipherText, 0, cipherText.Length);

            return cipherText;
        }

        protected void CheckArguments(byte[] originalPayload, byte[] key)
        {
            if (originalPayload == null || originalPayload.Length <= 0)
            {
            }

            if (key == null || key.Length <= 0)
            {
            }
        }

        protected abstract int IvSize { get; }
    }
}
