﻿using NUnit.Framework;

namespace Basic
{
    [TestFixture]
    public class NullableTests
    {
        [Test]
        public void CompareNullable()
        {
            int? a = 1;
            int? b = 1;

            var result = a==b;

            Assert.IsTrue(result);
        }

        [Test]
        public void CompareNullableByValue()
        {
            int? a = 1;
            int? b = 1;

            var result = a.Value == b.Value;

            Assert.IsTrue(result);
        }


        [Test]
        public void CompareNullableByExtractedValue()
        {
            int? a = 1;
            int? b = 1;

            var av=a.Value;
            var bv = b.Value;

            var result = av == bv;

            Assert.IsTrue(result);
        }

        [Test]
        public void PassAndModifyNullableInMethod()
        {
            int? a =2;

            ChangeIt(a);

            Assert.That(a, Is.EqualTo(2));
        }

        [Test]
        public void PassAndModifyNullableAsObjectInMethod()
        {
            int? a = 2;

            ChangeIt(a);

            Assert.That(a, Is.EqualTo(2));
        }

        private static void ChangeIt(int? a)
        {
            a = 3;
        }

        private static void ChangeItAsObject(object a)
        {
            a = 3;
        }
    }
}
