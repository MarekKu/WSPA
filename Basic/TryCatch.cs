﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Basic
{
    [TestFixture]
    public class TryCatch
    {
        [Test]
        public void SideEffectTests()
        {
            var a = 3;
            try
            {
                a=5;
                var c=0;
                var b = 1/c;
            }
            catch (Exception) { }

            Assert.That(a, Is.EqualTo(5));
        }
    }
}
