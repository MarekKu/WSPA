﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Basic
{
    [TestFixture]
    public class StringPassedToMethod
    {
        [Test]
        public void t()
        {
            string a ="a";

            testA(a);

            Assert.That(a == "a");
        }


        public void testA(string a)
        {
            a = "c";
        }
    }
}
