﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Parallela
{
    public class Program
    {
        static int[] resultStatic =  new int[100000];

      //  [Benchmark]
        public void SquareEachValue()
        {
            const int count = 100000;
            var values = Enumerable.Range(0, count);
            var results = new int[count];
            Parallel.ForEach(values, x => { results[x] = (int)Math.Pow(x, 2); });
        }

        //[Benchmark]
        public void SquareEachValueWithoutLambda()
        {
            const int count = 100000;
            var values = Enumerable.Range(0, count);
            var results = new int[count];
            Parallel.ForEach(values, Convert);
        }

        public static void Convert(int x)
        {
            resultStatic[x] = (int)Math.Pow(x, 2); 
        }

     //   [Benchmark]
        public void SquareEachValueChunked()
        {
            const int count = 100000;
            var values = Enumerable.Range(0, count);
            var results = new int[count];
            var part = Partitioner.Create(0, count, 10000); // rangeSize = size of each subrange

            Parallel.ForEach(part, range =>
            {
                for (int i = range.Item1; i < range.Item2; i++)
                {
                    results[i] = (int)Math.Pow(i, 2);
                }
            });
        }

        public static async Task<int> Calcualte()
        {
            Console.WriteLine("calculate started" + DateTime.Now);
            await Task.Delay(6000);
            Console.WriteLine("calcualte completed" + DateTime.Now);
            return 123;
        }

        static void Main()
        {
            Task.Factory.StartNew(NewMethod);

            //var items = Enumerable.Range(1, 50).ToArray();
            //var cubes = items.AsParallel().AsOrdered().Select(x =>
            //{
            //    Console.WriteLine($"{ x} - {Task.CurrentId}");
            //    return x * x * x;
            //});

            //var sum = ParallelEnumerable.Range(1, 10).Sum();
            //Console.WriteLine();
            ////Console.WriteLine(cubes.Any());
            //foreach (var c in cubes.Skip(46))
            //    Console.WriteLine(c);


            //var sw = new Stopwatch();
            //for(int i=0; i<10; i++)
            //{
            //    new Program().SquareEachValue();
            //}
            //sw.Start();
            //new Program().SquareEachValue();
            //sw.Stop();
            //Console.WriteLine(sw.ElapsedMilliseconds);
            //for(int i=0; i<10; i++)
            //{
            //    new Program().SquareEachValueWithoutLambda();
            //}
            //sw.Reset();
            //sw.Start();
            //new Program().SquareEachValueWithoutLambda();
            //sw.Stop();
            //Console.WriteLine(sw.ElapsedMilliseconds);

            //var summary = BenchmarkRunner.Run<Program>();
            //Console.WriteLine(summary);

            Console.ReadKey();
        }

        private static async Task NewMethod()
        {
            Console.WriteLine("start" + DateTime.Now);
            var resultTask = Calcualte();
            Thread.Sleep(3000);
            Console.WriteLine("in progres" + DateTime.Now);
            Console.WriteLine(await resultTask);
        }

        static void OlderMain(string[] args)
        {
            // add numbers from 1 to 100

            int sum = 0;
            int counter = 0;
           
            sum = 0;
            var po = new ParallelOptions();
            var sw = new Stopwatch();
            sw.Start();
            po.MaxDegreeOfParallelism = 16;
            Parallel.For(1, 10001, po,
              () => 0, // initialize local state, show code completion for next arg
              (x, state, tls) =>
              {
            //Console.WriteLine($"{Task.CurrentId}");
            tls += x;
                  Thread.Sleep(5);
                  //Console.WriteLine($"Task {Task.CurrentId} has sum {tls}");
                  return tls;
              },
              partialSum =>
              {
                  Interlocked.Increment(ref counter);
                 // Console.WriteLine($"Partial value of task {Task.CurrentId} is {partialSum}");
                  Interlocked.Add(ref sum, partialSum);
              }
            );
            Console.WriteLine($"Sum of 1..100 = {sum}");
            Console.WriteLine($"counter: {counter}");
            sw.Stop();
            Console.WriteLine($"elapsed: {sw.ElapsedMilliseconds}");

            Console.ReadKey();
        }



        static void SecondOldMain(string[] args)
        {
            ThreadPool.SetMaxThreads(1,1);
            var po = new ParallelOptions();
            po.MaxDegreeOfParallelism = 4;
            Parallel.For(1, 20, po,  x =>{
                Console.WriteLine($"{x} [{Task.CurrentId}] {Thread.CurrentThread.ManagedThreadId} started");
                Thread.Sleep(1000);
               // Thread.SpinWait(100000000);
                Console.WriteLine($"{x} [{Task.CurrentId}] {Thread.CurrentThread.ManagedThreadId} ended");
            });

            Console.ReadKey();
        }


        static void OldMain(string[] args)
        {
            var cts = new CancellationTokenSource();

            //cts.Token.Register(() =>
            //{
            //    Console.WriteLine("cancalation requested");
            //});

            //var t = new Task(() =>
            //{
            //    int i = 0;

            //    while(true)
            //    {

            //        cts.Token.ThrowIfCancellationRequested();     
            //            Console.WriteLine($"Number {i++} currentId {Task.CurrentId}\n");
            //            Console.WriteLine($"can be cancaled: {cts.Token.CanBeCanceled}");
            //    }

            //});
            var t = new Task(() =>
            {
                throw new InvalidCastException();
                cts.Token.ThrowIfCancellationRequested();
                //for (int i = 0; i < 5; i++)
                //{
                //    cts.Token.ThrowIfCancellationRequested();
                //    Thread.Sleep(1000);
                //}
            }, cts.Token);
            t.Start();


            var t2 = new Task(() =>
            {
                Thread.Sleep(1000);
                cts.Token.ThrowIfCancellationRequested();
                
                Console.WriteLine("t2 done");
                //cts.Cancel();
            }, cts.Token);
            t2.Start();


            var t3 = new Task<int>(() => throw new ExecutionEngineException());
            t3.Start();

            var z = t3.Result;
           // Thread.Sleep(1000);
            cts.Cancel();
            try
            {
                Task.WaitAll(t, t2);
            }
            catch (InvalidCastException ex)
            {
                Console.WriteLine("kto zmienia avatar");
            }
            catch(AggregateException ex)
            {
                Console.WriteLine($"status: {t.Status}");
                Console.WriteLine($"status: {t2.Status}");
            }
            //Thread.Sleep(1000);
            //t.Wait();
             // t.Wait(cts.Token);
            //Console.ReadKey();
            //cts.Cancel();


            Console.WriteLine($"ended: {Task.CurrentId}");
            Console.WriteLine($"can be cancaled: {cts.Token.CanBeCanceled}");
            Console.ReadKey();
        }
    }


    struct AStruct
    {
        //public  a;
         //IEnumerable<int>
        public int counter;
        public void AM() {
            counter = 1_00_00;
        }
    }

   }
