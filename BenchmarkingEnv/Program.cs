﻿using System;
using BenchmarkDotNet.Running;
using MyBenchmarks;

namespace BenchmarkingEnv
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var summary = BenchmarkRunner.Run<Md5vsSha256v2>();
            Console.Read();
        }
    }
}
