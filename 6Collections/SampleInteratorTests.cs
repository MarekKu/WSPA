﻿using NUnit.Framework;
using System.Collections.Generic;

namespace Collections
{
    [TestFixture]
    public class SampleInteratorTests
    {
        [Test]
        public void IteratorWithForeach()
        {
            var resutl = "";
            foreach (var a in SampleIterator.IteratorWithForeach(new[] { "a", "b", "a", "c"}))
            {
                resutl += a;
            }
        }


        [Test]
        public void SampleEnumerator()
        {
            var resutl = "";
            foreach (var c in SampleIterator.SimpleIterator())
            {
                resutl += c;
                if (resutl.Length == 2)
                {
                    break;
                }
            }

            Assert.That(resutl, Is.EqualTo("ab"));
        }

        [Test]
        public void ConsumeEnumerator1()
        {
            var resutl = "";
            var r = new List<int>(new[] { 1, 2, 3 });//SampleIterator.Generate(1, 2, 3);

            using (var enumerator = r.GetEnumerator())
                while(enumerator.MoveNext())
                {
                    var element = enumerator.Current;
                    resutl += element;
                }

            Assert.That(resutl, Is.EqualTo("test"));
        }

        [Test]
        public void ConsumeEnumerator2()
        {
            var resutl = "";
            foreach(var c in "test")
            {
                resutl += c;
            }
            
            Assert.That(resutl, Is.EqualTo("test"));
        }

        [Test]
        public void Generetest()
        {
            var l = SampleIterator.Generate(1, 2, 2);
            foreach(var z in l)
            {
                break;
            }
        }


        [Test]
        public void ProgressionTests()
        {
            var numberOfElements = 103;
            List<int> list = new List<int>();
            var coll = SampleIterator.Generate2(1, 2, count: numberOfElements);
            foreach (var z in coll)
            {
                list.Add(z);
                //if (list.Count == 5) break;
            }

            //Assert.That(list.Count, Is.EqualTo(5));
        }

        [Test]
        public void ProgressionTests2()
        {
            var result = SampleIterator.Generate2(1, 2, 3);
            foreach(var z in result)
            {
                Assert.That(z, Is.EqualTo(1));
                break;
            }

          //  Assert.That(result, Is.EqualTo(new[] { 1, 3, 5 }));
        }

        [Test]
        public void SequenceTest()
        {
            var result = string.Empty;

            foreach (int e in SampleIterator.EvenNumbers(SampleIterator.GenerateFibonacci(6)))
            {
                result += e;
            }

            Assert.That(result, Is.EqualTo("28"));
        }
    }
}
