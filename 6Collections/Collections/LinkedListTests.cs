﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Collections.Collections
{
    [TestFixture]
    class LinkedListTests
    {
        //konstruktor
        [Test]
        public void GivenAnArray_WhenPassingArrayToLinkedListConstructor_LinkedListIsCreated()
        {
            var array = new[] { 1, 2, 3 };

            var list = new LinkedList<int>(array);

            Assert.That(list.Count, Is.EqualTo(array.Length));
        }

        //Add Elements
        [Test]
        public void GivenALinkedList_WhenAddingElement_ElementsAreAdded()
        {
            var list = new LinkedList<int>(new[] { 1, 3, 4 });
            
            list.AddAfter(list.First, 2);

            Assert.That(list, Has.Count.EqualTo(4));
            Assert.That(list, Is.EqualTo(new[] { 1, 2, 3, 4}));
        }

        //Remove Elements
        [Test]
        public void GivenALinkedList_WhenRemovingElement_ElementIsRemoved()
        {
            var list = new LinkedList<int>(new[] { 1, 2, 3, 4 });

            list.Remove(2);

            Assert.That(list, Has.No.Member(2));
        }

        //reading first and last element
        [Test]
        public void GivenALinkedList_CanFastReadFirstAndLastElement()
        {
            var list = new LinkedList<int>(new[] { 1, 2, 3, 4 });

            Assert.That(list.First.Value, Is.EqualTo(1));
            Assert.That(list.Last.Value, Is.EqualTo(4));
        }

        //iterating over linkedlist (without foreach)
        [Test]
        public void GivenALinkedListElement_CanIteretOverLinkedList()
        {
            var list = new LinkedList<int>(new[] { 1, 2, 3, 4 });
            var firstElement = list.First;

            var thirdElemnt = firstElement.Next.Next;
            var secondElement = thirdElemnt.Previous;

            Assert.That(thirdElemnt.Value, Is.EqualTo(3));
            Assert.That(secondElement.Value, Is.EqualTo(2));
        }


        //kopiowanie
        [Test]
        public void GivenALinkedList_WhenCopy_LinkedListIsCopied()
        {
            var list = new LinkedList<int>(new[] { 1, 2, 3, 4, 5 });
            var resultArray = new[] { 1, 4, 5, 6, 7 };

            list.CopyTo(resultArray, 0);

            Assert.That(resultArray, Is.EqualTo(new[] { 1, 2, 3, 4, 5 }));
        }

        [Test]
        public void GivenAList_CheckCapacity()
        {
            var list = new LinkedList<int>(new[] { 1, 2, 3, 4, 5 });

            TimeSpan result = AddManyElementToList(list);
        }

        private static TimeSpan AddManyElementToList(LinkedList<int> list)
        {
            var t1 = DateTime.UtcNow;
            for (int i = 0; i < 1000000; i++)
            {
                list.AddFirst(i);
            }
            var t2 = DateTime.UtcNow;

            var result = t2 - t1;
            return result;
        }
        //porówanie czasu działania linkedList a list (na końcu)
    }
}
