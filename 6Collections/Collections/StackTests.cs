﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Collections.Collections
{
    [TestFixture]
    class StackTests
    {
        //Add/Remove elements
        [Test]
        public void GivenAStack_CanPushAndPoplement()
        {
            var stack = new Stack<int>();
            stack.Push(1);
            stack.Push(2);

            Assert.That(stack.Count, Is.EqualTo(2));

            Assert.That(stack.Peek, Is.EqualTo(2));

            Assert.That(stack.Pop(), Is.EqualTo(2));
            Assert.That(stack.Pop(), Is.EqualTo(1));
        }

        //konstruktor
        [Test]
        public void CanConstructStackFromArray()
        {
            var stack = new Stack<int>(new int[] { 1, 2, 3 });

            Assert.That(stack.Pop(), Is.EqualTo(3));
            Assert.That(stack.Pop(), Is.EqualTo(2));
        }

        //Cleear
        [Test]
        public void GivenAStack_WhenClearing_StackIsEmpty()
        {
            var stack = new Stack<int>(new int[] { 1, 2, 3 });

            stack.Clear();

            Assert.That(stack.Count, Is.EqualTo(0));
        }

        //kopiowanie
        [Test]
        public void GivenAStack_CanCopyToArray()
        {
            var stack = new Stack<int>(new int[] { 1, 2, 3 });
            var resultArray = new[] { 1, 4, 5, 6 };

            stack.CopyTo(resultArray, 0);

            Assert.That(resultArray, Is.EqualTo(new[] { 3, 2, 1, 6 }));
        }


        //Capacity
        [Test]
        public void NoCapacitySet()
        {
            var stack = new Stack<int>(2);

            var result = AddManyElementToStack(stack);
        }

        [Test]
        public void SetStackCapacity()
        {
            var stack = new Stack<int>(1000000);

            var result = AddManyElementToStack(stack);
        }

        private static TimeSpan AddManyElementToStack(Stack<int> stack)
        {
            var t1 = DateTime.UtcNow;
            for (int i = 0; i < 1000000; i++)
            {
                stack.Push(i);
            }
            var t2 = DateTime.UtcNow;

            var result = t2 - t1;
            return result;
        }


    }
}
