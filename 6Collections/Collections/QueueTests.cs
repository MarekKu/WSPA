﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Collections.Collections
{
    [TestFixture]
    class QueueTests
    {
        //Add/Remove elements
        [Test]
        public void GivenAQueue_CanEnqueueAndDequeueElement()
        {
            var queue = new Queue<int>();
            queue.Enqueue(1);
            queue.Enqueue(2);

            Assert.That(queue.Count, Is.EqualTo(2));

            Assert.That(queue.Peek, Is.EqualTo(1));

            Assert.That(queue.Dequeue, Is.EqualTo(1));
            Assert.That(queue.Dequeue, Is.EqualTo(2));
        }

        //konstruktor
        [Test]
        public void CanConstructQueueFromArray()
        {
            var queue = new Queue<int>(new int[] { 1, 2, 3 });

            Assert.That(queue.Dequeue, Is.EqualTo(1));
            Assert.That(queue.Dequeue, Is.EqualTo(2));
        }

        //Cleear
        [Test]
        public void GivenAQueue_WhenClearing_QueueIsEmpty()
        {
            var queue = new Queue<int>(new int[] { 1, 2, 3 });

            queue.Clear();

            Assert.That(queue.Count, Is.EqualTo(0));
        }

        //kopiowanie
        [Test]
        public void GivenAQueue_CanCopyToArray()
        {
            var queue = new Queue<int>(new int[] { 1, 2, 3 });
            var resultArray = new[] { 1, 4, 5, 6};

            queue.CopyTo(resultArray, 0);

            Assert.That(resultArray, Is.EqualTo(new[] { 1, 2, 3, 6 }));
        }


        //Capacity
        [Test]
        public void NoCapacitySet()
        {
            var queue = new Queue<int>(2);

            var result = AddManyElementToQueue(queue);
        }

        [Test]
        public void SetQueueCapacity()
        {
            var queue = new Queue<int>(1000000);

            var result = AddManyElementToQueue(queue);
        }

        private static TimeSpan AddManyElementToQueue(Queue<int> queue) 
        {
            var t1 = DateTime.UtcNow;
            for (int i = 0; i < 1000000; i++)
            {
                queue.Enqueue(i);
            }
            var t2 = DateTime.UtcNow;

            var result = t2 - t1;
            return result;
        }



    }
}
