﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Collections.Collections
{
    [TestFixture]
    class SortedSetTests
    {
        [Test]
        public void CanDetermineMaxAndMin()
        {
            var set = new SortedSet<int>(new[] { 2, 3, 1 });

            Assert.That(set.Max, Is.EqualTo(3));
            Assert.That(set.Min, Is.EqualTo(1));
        }

        [Test]
        public void CanReverse()
        {
            var set = new SortedSet<int>(new[] { 2, 3, 3, 1 });
            Assert.That(set, Is.EqualTo(new[] { 1, 2, 3 }));

            var reversed = set.Reverse();

            Assert.That(reversed, Is.EqualTo(new[] { 3, 2, 1 }));
        }

        [Test]
        public void CanGetView()
        {
            var set = new SortedSet<int>(new[] { 2, 3, 1, 4 });

            var view = set.GetViewBetween(2, 3);

            Assert.That(view, Is.EqualTo(new[] { 2, 3 }));
        }

        [Test]
        public void CustomComparer()
        {
           
            var set = new SortedSet<int>(new[] { 2, 3, 1 }, new CustomComparer());
            set.Add(4);

            Assert.That(set.Max, Is.EqualTo(1));
            Assert.That(set.Min, Is.EqualTo(4));
        }
    }

    public class CustomComparer : IComparer<int>
    {
        public int Compare(int x, int y)
        {
            if (x == y) return 0;
            return x > y ?
                1 :
                -1;
        }
    }
}
