﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Collections.Collections
{
    public class Person
    {
        public Person(int Id)
        {
            this.Id = Id;
        }
        public int Id { get; set; }

        public override bool Equals(object obj)
        {
            var o = obj as Person;
            return o.Id.Equals(this.Id);
        }
    }


    [TestFixture]
    class HashSetTests
    {
        //konstruktor
        [Test]
        public void AGivenAArray_WhenCreatingHashSet_DuplicatesAreAutomaticalyRemoved()
        {
            var hashSet = new HashSet<Person>();
            hashSet.Add(new Person(2));
            hashSet.Add(new Person(2));

            Assert.That(hashSet.Count, Is.EqualTo(2));
        }

        [Test]
        public void AAGivenAArray_WhenCreatingHashSet_DuplicatesAreAutomaticalyRemoved()
        {
            var hashSet = new HashSet<Person>();
            var person = new Person(2);
            hashSet.Add(person);
            hashSet.Add(person);

            Assert.That(hashSet.Count, Is.EqualTo(2));
        }


        //konstruktor
        [Test]
        public void GivenAArray_WhenCreatingHashSet_DuplicatesAreAutomaticalyRemoved()
        {
            var array = new[] { 1, 1, 2, 3, 3, 3 };

            var hashSet = new HashSet<int>(array);

            Assert.That(hashSet, Is.EqualTo(new[] { 1, 2, 3 }));
        }

        //Operacje na zbiorach
        [Test]
        public void CanUnion()
        {
            var hashSet1 = new HashSet<int>(new[] { 1, 2 });
            var hashSet2 = new HashSet<int>(new[] { 2, 3 });

            hashSet1.UnionWith(hashSet2);

            Assert.That(hashSet1, Is.EqualTo(new[] { 1, 2, 3 }));
        }

        [Test]
        public void CaniIntersect()
        {
            var hashSet1 = new HashSet<int>(new[] { 1, 2 });
            var hashSet2 = new HashSet<int>(new[] { 2, 3 });

            hashSet1.IntersectWith(hashSet2);

            Assert.That(hashSet1, Is.EqualTo(new[] { 2 }));
        }

        [Test]
        public void CanExcept()
        {
            var hashSet1 = new HashSet<int>(new[] { 1, 2 });
            var hashSet2 = new HashSet<int>(new[] { 2, 3 });

            hashSet1.ExceptWith(hashSet2);

            Assert.That(hashSet1, Is.EqualTo(new[] { 1 }));
        }

        [Test]
        public void CanSymetricExcept()
        {
            var hashSet1 = new HashSet<int>(new[] { 1, 2 });
            var hashSet2 = new HashSet<int>(new[] { 2, 3 });

            hashSet1.SymmetricExceptWith(hashSet2);

            Assert.That(hashSet1, Is.EqualTo(new[] { 1, 3 }));
        }

        //metody sprawdzające zbiór
        [Test]
        public void CompareSubSet()
        {
            var hashSet1 = new HashSet<int>(new[] { 1, 2 });
            var hashSet2 = new HashSet<int>(new[] { 2, 3 });

            Assert.True(hashSet1.IsSubsetOf(new[] { 1, 2 }));
            Assert.False(hashSet1.IsProperSubsetOf(new[] { 1, 2 }));
            Assert.True(hashSet1.IsProperSubsetOf(new[] { 1, 2, 3 }));

            Assert.True(hashSet1.IsSupersetOf(new[] { 1 }));
            Assert.False(hashSet1.IsProperSupersetOf(new[] { 1, 2 }));
            Assert.True(hashSet1.IsProperSupersetOf(new[] { 1 }));

            Assert.True(hashSet1.Overlaps(new[] { 2, 3 }));
            Assert.False(hashSet1.Overlaps(new[] { 3, 4 }));

            Assert.True(hashSet1.SetEquals(new[] { 1, 2 }));
        }
    }
}
