﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Collections.Collections
{
    [TestFixture]
    class ArrayTests
    {
        string[] stringArray = new[] { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
            "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };

        [Test]
        public void ArrayForEach()
        {
            var array = new[] { 1, 2, 3 };
            var result = "";

            Array.ForEach(array, x => result += x);

            Assert.That(result, Is.EqualTo("123"));
        }

        //przeszukiwanie tablic

        [Test]
        public void IndexOf_LastIndex()
        {
            var result = Array.LastIndexOf(stringArray, "seven", 9, 4);

            Assert.That(result, Is.EqualTo(7));
        }

        [Test]
        public void FindLast()
        {
            var result = Array.FindLast(stringArray, x => x.StartsWith("s"));

            Assert.That(result, Is.EqualTo("seven"));
        }

        [Test]
        public void FindAll()
        {
            var result = Array.FindAll(stringArray, x => x.Length == 5);

            Assert.That(result, Is.EqualTo(new[] { "three", "seven", "eight", "three", "seven", "eight" }));
        }

        [Test]
        public void Exists_TrueForAdd()
        {
            var exists = Array.Exists(stringArray, x => x == "zero");
            var all = Array.TrueForAll(stringArray, x => x.Length >= 3);

            Assert.True(exists);
            Assert.True(all);
        }

        [Test]
        public void BinarySearch()
        {
            var array = new[] { 1, 2, 3 };

            var result = Array.BinarySearch(array, 2);

            Assert.That(result, Is.EqualTo(1));
        }
        //konwertowanie
        [Test]
        public void Convert()
        {
            string[] array = new string[4];
            Array.Copy(stringArray, array, 4);

            var re = Array.ConvertAll(array, x =>
            {
                return new Person(x.Length);
            });

            var ids = Array.ConvertAll(re, x => x.Id);
            //var result = Array.ConvertAll(array, x => x.Length);

            Assert.That(ids, Is.EqualTo(new[] { 4, 3, 3, 5 }));
        }

        //sorotwanie
        [Test]
        public void Sorting()
        {
            var a = new A() { MyProperty = "a" };
            var b = new A() { MyProperty = "bb" };
            var c = new A() { MyProperty = "ccc" };

            var array = new[] { b, c, a };

            //Array.Sort(array, (x, y) => x.MyProperty.CompareTo(y.MyProperty));
            //Array.Sort(array);
            Array.Sort(array, new compare());


            Assert.That(array[0].MyProperty, Is.EqualTo("a"));
            Assert.That(array[2].MyProperty, Is.EqualTo("ccc"));

            //Assert.That(array[0].MyProperty, Is.EqualTo("ccc"));
            //Assert.That(array[2].MyProperty, Is.EqualTo("a"));
        }

        static int Compare(A a, A b) //To samo zapisać lambdą, IComparable<T> i pomocniczego obiektu IComparer<T>
        {
            return a.MyProperty.CompareTo(b.MyProperty);
        }


        [Test]
        public void SortTwoArrays()
        {
            int[] numbers = { 3, 2, 1 };
            string[] words = { "trzy", "dwa", "jeden" };

            Array.Sort(numbers, words);

            Assert.That(numbers, Is.EqualTo(new[] { 1, 2, 3 }));
            Assert.That(words, Is.EqualTo(new[] { "jeden", "dwa", "trzy" }));
        }

    }

    class compare : IComparer<A>
    {
        public int Compare(A x, A y)
        {
            return x.MyProperty.CompareTo(y.MyProperty);
        }
    }

    class A : IComparable<A>
    {
        public string MyProperty { get; set; }
        public int Number { get; set; }

        public int CompareTo(A other)
        {
            return -this.MyProperty.CompareTo(other.MyProperty);
            //throw new NotImplementedException();
        }
    }

}
