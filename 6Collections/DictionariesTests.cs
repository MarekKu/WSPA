﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Collections
{
    public class WithoutHashCode
    {
        public int MyProperty { get; set; }
    }

    public class WithHashCode
    {
        public int MyProperty { get; set; }

        public override int GetHashCode()
        {
            return MyProperty;
        }
        public override bool Equals(object obj)
        {
            return Equals(obj as WithHashCode);
        }

        public bool Equals(WithHashCode obj)
        {
            return obj != null && obj.MyProperty == this.MyProperty;
        }
    }

    [TestFixture]
    public class DictionariesTests
    {
        [Test]
        public void TestClassAsDictionary()
        {
            var dict = new Dictionary<WithoutHashCode, int>();
            dict[new WithoutHashCode { MyProperty = 1}] = 1;
            dict[new WithoutHashCode { MyProperty = 1}] = 2;

            var result = new List<int>();
            foreach(var key in dict.Keys)
            {
                result.Add(dict[key]);
            }

            Assert.That(result, Is.EqualTo(new[] { 1,2}));
        }

        [Test]
        public void TestClassWithHashCodeAsDictionary()
        {
            var dict = new Dictionary<WithHashCode, int>();
            dict[new WithHashCode { MyProperty = 1 }] = 1;
            dict[new WithHashCode { MyProperty = 1 }] = 2;

            var result = new List<int>();
            foreach (var key in dict.Keys)
            {
                result.Add(dict[key]);
            }

            Assert.That(result, Is.EqualTo(new[] { 2 }));
        }
    }
}
