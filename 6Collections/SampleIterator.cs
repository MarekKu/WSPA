﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collections
{
    class SampleIterator  
    {
        public static IEnumerable<string> IteratorWithForeach(string[] inputs)
        {
            foreach(var i in inputs)
            {
                if(i == "a")
                yield return i+"a";

                Console.WriteLine(i);
            }
        }

        public static IEnumerable<string> SimpleIterator()
        {
            yield return "a";
            yield return "b";
            yield return "c";

            throw new Exception();
        }

        public static IEnumerable<int> Generate(int a, int q, int count)
        {
            var z = 1; 
            for (int i = 0; i < count; i++)
                yield return a + i*q;
            z = 2;
        }
        // wywołane z count=3 jest wyjąek, z count = 103 nie ma wyjatku
        public static IEnumerable<int> Generate2(int a, int q, int count)
        {
            for (int i = 0; i < count; i++)
            {
                yield return a + i * q;
                if(i>=100)
                {
                    yield break;
                }
            }
            throw new Exception();
        }

        public static IEnumerable<int> GenerateFibonacci(int count)
        {
            for (int i = 0, previous = 1, current = 1; i< count; i++)
            {
                yield return previous;
                int next = previous+current;
                previous = current;
                current = next;
            }
        }

        public static IEnumerable<int> EvenNumbers(IEnumerable<int> enumerable)
        {
            foreach (int e in enumerable)
            {
                if(e%2 == 0)
                {
                    yield return e;
                }
            }
        }

    }

    class A { }
    class B :A { }
    class C :A { }
}
