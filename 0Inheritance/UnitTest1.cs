﻿using NUnit.Framework;
using System;

namespace _0Inheritance
{
    [TestFixture]
    public class UnitTest1
    {
        [Test]
        public void TestMethod1()
        {
            Assert.That(-7, Is.EqualTo(Math.Round(-6.5, MidpointRounding.AwayFromZero)));
            Assert.That(7, Is.EqualTo(Math.Round(6.5, MidpointRounding.AwayFromZero)));
        }

        [Test]
        public void Date()
        {
            var dt = new DateTime(2016, 2, 29);
            var result = dt.AddYears(1);
            Assert.That(result.Day, Is.EqualTo(28));

            var result2 = new DateTime(dt.Year + 1, dt.Month, dt.Day);

        }

        [Test]
        public void Date2()
        {
            var dt = new DateTime(2016, 2, 29);
             dt = dt.AddYears(1);
             dt = dt.AddMonths(1);
             dt = dt.AddDays(-1);
             dt = dt.AddHours(-1);

            var result2 = new DateTime(dt.Year + 1, dt.Month, dt.Day);
        }
    }
}
