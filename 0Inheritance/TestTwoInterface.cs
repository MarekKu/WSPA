﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _0Inheritance
{
    interface TestA
    {
        void Add();
    }

    interface TestB
    {
        void Add();
    }

    class TestTwoInterface : TestA, TestB
    {
        public void Add()
        {
            throw new NotImplementedException();
        }
    }
}
