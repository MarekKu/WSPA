﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;

namespace _6RegularExpresion
{
    [TestFixture]
    public class BasicRegexTests
    {
        [Test]
        public void Kwantyfikator()
        {
            var pattern = @"colou?r";

            Assert.IsTrue(Regex.Match("color", pattern).Success);
            Assert.IsTrue(Regex.Match("colour", pattern).Success);
            Assert.IsFalse(Regex.Match("colouur", pattern).Success);
        }

        [Test]
        public void WynikRegexMatch()
        {
            var pattern = @"colou?r";

            Match m = Regex.Match("I like blue color", pattern);

            Assert.IsTrue(m.Success);
            Assert.That(m.Index, Is.EqualTo(12));
            Assert.That(m.Length, Is.EqualTo(5));
            Assert.That(m.Value, Is.EqualTo("color"));
            Assert.That(m.ToString(), Is.EqualTo("color"));

        }

        [Test]
        public void IsMatch()
        {
            var pattern = @"colou?r";

            Assert.IsTrue(Regex.IsMatch("color", pattern));
        }

        [Test]
        public void NextMatch()
        {
            var pattern = @"colou?r";

            Match m = Regex.Match("I like blue color. I like red colour.", pattern);
            m = m.NextMatch();

            Assert.That(m.Value, Is.EqualTo("colour"));
        }

        [Test]
        public void IteratingOverAllMatches()
        {
            var pattern = @"colou?r";
            var result = new List<string>();
            foreach (Match m in Regex.Matches("I like blue color. I like red colour.", pattern))  //?studenci dlaczego Match a nie var
            {
                result.Add(m.Value);
            }

            Assert.That(result, Is.EqualTo(new[] { "color", "colour" }));
        }

        [Test]
        public void Alternatywa()
        {
            var pattern = @"Karol|Karolina";

            Assert.IsTrue(Regex.IsMatch("Karol", pattern));
            Assert.IsTrue(Regex.IsMatch("Karolina", pattern));
        }

        [Test]
        public void AlternatywaPodzbior()
        {
            var pattern = "Kar(ol|olina)";

            Assert.IsTrue(Regex.IsMatch("Karol", pattern));
            Assert.IsTrue(Regex.IsMatch("Karolina", pattern));
        }

        [Test]
        public void CompiledRegex()
        {
            var pattern = @"colou?r";
            Regex r = new Regex(pattern, RegexOptions.Compiled);

            var result = r.Match("color");

            Assert.IsTrue(result.Success);
        }
    }

    [TestFixture]
    public class SpecialCharacters
    {
        [Test]
        public void EscapeSpecialCharacter()
        {
            var text = "co?";

            Assert.That(Regex.Match(text, @"co\?").Value, Is.EqualTo("co?"));
            Assert.That(Regex.Match(text, @"co?").Value, Is.EqualTo("co"));
        }

        [Test]
        public void RegexEscape()
        {
            var pattern = "co?";
            var text = "co?";

            Assert.That(Regex.Match(text, Regex.Escape(pattern)).Value, Is.EqualTo("co?"));
        }

        [Test]
        public void RegexUnescape()
        {
            var pattern = @"co\?";

            Assert.That(Regex.Unescape(pattern), Is.EqualTo("co?"));
        }
    }

    [TestFixture]
    public class CharactersSet
    {
        [Test]
        public void CharacterList1()
        {
            Assert.True(Regex.IsMatch("c0-f9", @"[a-h]\d-[a-h]\d"));
            Assert.False(Regex.IsMatch("c0-f9", @"[a-h][1-8]-[a-h][1-8]"));
        }

        [Test]
        public void CharacterList()
        {
            var pattern = @"[abcd]";
            var z = Regex.Matches("abfcd", pattern);
           

            Assert.True(Regex.IsMatch("a", pattern));
            Assert.That(Regex.Matches("abfcd", pattern).Count, Is.EqualTo(4));
            Assert.False(Regex.IsMatch("f", pattern));

            pattern = @"[a-d]";

            Assert.True(Regex.IsMatch("a", pattern));
            Assert.That(Regex.Matches("abcd", pattern).Count, Is.EqualTo(4));
            Assert.False(Regex.IsMatch("f", pattern));

            //q not followed by vovel
            Assert.That(Regex.Match("quiz qwerty", @"q[^aeiou]").Index, Is.EqualTo(5));

            //chess move
            Assert.True(Regex.IsMatch("c3-f5", @"[a-h]\d-[a-h]\d"));
            Assert.False(Regex.IsMatch("c0-f9", @"[a-h][1-8]-[a-h][1-8]"));
        }

        [Test]
        public void Digit_FindPercent()
        {
            var pattern = @" \d{1,2}%";
            var text = "In 2017 GDP has increased 234%";

            Assert.That(Regex.Match(text, pattern).Value, Is.EqualTo("234"));
        }

        [Test]
        public void Dot()
        {
            var pattern = "a.c";

            Assert.True(Regex.IsMatch("abc", pattern));
            Assert.True(Regex.IsMatch("a9c", pattern));
            Assert.True(Regex.IsMatch("a c", pattern));
        }
    }

    [TestFixture]
    public class Quantifiers
    {
        [Test]
        public void QuantifiersTest()
        {
            //*
            var pattern = @"cv\d*\.doc";
            Assert.True(Regex.IsMatch("cv.doc", pattern));
            Assert.True(Regex.IsMatch("cv14.doc", pattern));

            //+
            pattern = @"cv\d+\.doc";
            Assert.False(Regex.IsMatch("cv.doc", pattern));
            Assert.True(Regex.IsMatch("cv14.doc", pattern));

            //{} blood pressure
            pattern = @"\d{2,3}/\d{2,3}";
            Assert.That(Regex.Match("w wieku 70 cisnienie wynosci 140/80", pattern).Value, Is.EqualTo("140/80"));
        }

        [Test]
        public void LazyAndGreedyQuantifier()
        {
            string xml = @"<deviceName>turbine</deviceName><deviceName>engine</deviceName>";

            Assert.That(Regex.Match(xml, @"<deviceName>.*</deviceName>").Value, Is.EqualTo(xml));
            Assert.That(Regex.Match(xml, @"<deviceName>.*?</deviceName>").Value, Is.EqualTo("<deviceName>turbine</deviceName>"));
        }
    }

    [TestFixture]
    public class AtomicZero_WidthAssertions
    {
        [Test]
        public void PositiveLookahead()
        {
            var text = "I 23 am 25 years old";
            var pattern = @"\d+(?=\syears?)";

            Assert.That(Regex.Match(text, pattern).Value, Is.EqualTo("25"));

            //pattern = @"\d+(?=\syears).*";
            //Assert.That(Regex.Match(text, pattern).Value, Is.EqualTo("25")); //zagadka, co będzie dopasowane

            //regex wymuszajacy hasło z przynajmniej 8 znakami i jedną cyfrą. 
        }

        [Test]
        public void NegativeLookahead()
        {
            var text = @"player1:0
player2:2
player3:0
player4:4";

            var pattern = @"player\d+(?!:0)";

            var matches = Regex.Matches(text, pattern);

            Assert.That(matches[0].Value, Is.EqualTo("player2"));
            Assert.That(matches[1].Value, Is.EqualTo("player4"));
        }

        [Test]
        public void PositiveLookbehind()
        {
            var text = @"Age=24,Name=Alice
Age=26,Name=Bob";

            var pattern = @"(?<=Age=[2-9][5-9],Name=).+";

            var matches = Regex.Matches(text, pattern);

            Assert.That(matches[0].Value, Is.EqualTo("Bob"));
            Assert.That(matches.Count, Is.EqualTo(1));
        }

        [Test]
        public void NegativeLookbehind()
        {
            var text = @"Length:30
Speed:40,
Power:50";

            var pattern = @"(?<!L.+)\d+";

            var matches = Regex.Matches(text, pattern);

            Assert.That(matches[0].Value, Is.EqualTo("40"));
            Assert.That(matches.Count, Is.EqualTo(2));
        }

        [Test]
        public void Anchors()
        {
            var text1 = "Ala miała kota";
            var text2 = "Ala ma kota w domu";

            var pattern = @"^Ala (ma|miała) kota$";

            Assert.True(Regex.IsMatch(text1, pattern));
            Assert.False(Regex.IsMatch(text2, pattern));
        }

        [Test]
        public void AnchorsMultiline()
        {
            var text = @"Zbyszek ma psa
Ala ma kota
Zuzia ma psa
Ola ma kota";

            var pattern = "kota$";
            var z = Regex.Matches(text, pattern, RegexOptions.Multiline);

            Assert.That(Regex.Matches(text, pattern, RegexOptions.Multiline).Count, Is.EqualTo(1));

            var pattern2 = "kota(?=\r?$)";

            Assert.That(Regex.Matches(text, pattern2, RegexOptions.Multiline).Count, Is.EqualTo(2));
        }

        [Test]
        public void WordBoundary2()
        {
            var text = "górze";
            var pattern = @"\w+";
            Assert.That(Regex.Match(text, pattern, RegexOptions.CultureInvariant).Value, Is.EqualTo("górze"));
        }

        [Test]
        public void LiczSlowa()
        {
            var wzorzecLiczbaSlow = @"\b\w";
            var liczbaSlow = "Tekst do policzenia ilości słów";
            var pasujace = Regex.Matches(liczbaSlow, wzorzecLiczbaSlow);
            Assert.That(pasujace.Count, Is.EqualTo(6));
        }

        [Test]
        public void WordBoundary()
        {

            var text = "Narty na górze";
            var pattern1 = @"\b[Nn]a\b";
            var pattern2 = @"[Nn]a";

            Assert.That(Regex.Matches(text, pattern1).Count, Is.EqualTo(1));
            Assert.That(Regex.Matches(text, pattern2).Count, Is.EqualTo(2));
        }
    }

    [TestFixture]
    public class RegexOptionsTests
    {
        [Test]
        public void IgnoreCase()
        {
            var pattern = @"colou?r";

            Assert.IsTrue(Regex.IsMatch("CoLor", pattern, RegexOptions.IgnoreCase));
        }

        [Test]
        public void Multiline()
        {
            var pattern = @"^colou?r\r$";
            var text = "col" + Environment.NewLine + "colour" + Environment.NewLine + "our";

            Assert.That(Regex.Matches(text, pattern).Count, Is.EqualTo(0));
            Assert.That(Regex.Matches(text, pattern, RegexOptions.Multiline).Count, Is.EqualTo(1));
        }

        [Test]
        public void ExplicitCapture()
        {
            Assert.Fail("po grupach");
        }

        [Test]
        public void Singleline()
        {
            var pattern = @"^.+";
            var text = "col" + Environment.NewLine + "colour" + Environment.NewLine + "our";

            Assert.That(Regex.Matches(text, pattern)[0].Value, Is.EqualTo("col\r"));
            Assert.That(Regex.Matches(text, pattern, RegexOptions.Singleline)[0].Value, Is.EqualTo(text));
        }

        [Test]
        public void IgnorePatternWhitespace()
        {
            var pattern = "Kar ( ol | olina )";

            Assert.IsFalse(Regex.IsMatch("Karol", pattern));
            Assert.IsTrue(Regex.IsMatch("Karol", pattern, RegexOptions.IgnorePatternWhitespace));
        }

        [Test]
        public void RightToLeft()
        {
            var pattern = "Kar(ol|olina)";
            var text = "Karol i Karolina";

            var matches = Regex.Matches(text, pattern, RegexOptions.RightToLeft);

            Assert.That(matches[0].Value, Is.EqualTo("Karolina"));
        }

        [Test]
        public void ECMAScript()
        {
            Assert.Fail("");
        }

        [Test]
        public void CultureInvariant()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("tr-TR");

            string pattern = "^file://";
            string inputFilePath = "FILE://C:/sample_file.txt";

            Assert.False(Regex.IsMatch(inputFilePath, pattern, RegexOptions.IgnoreCase));
            Assert.That(Regex.IsMatch(inputFilePath, pattern,
    RegexOptions.IgnoreCase | RegexOptions.CultureInvariant));

            
        }


        [Test]
        public void TurnOnAndOffRegexOptions()
        {
            var pattern = @"(?i)a(?-i)a";
            var text = "AAAa";

            Assert.That(Regex.Match(text, pattern).Value, Is.EqualTo("aa"));
        }
    }

    [Flags]
    enum test
    {
        one =1,
        two =2,
        t =3
    }

    [TestFixture]
    public class Groups
    {
        [Test]
        public void BasicGroups()
        {
            var tel = "mój numer to: 81-1234567. Proszę dzwonić";
            var pattern = @"(\d{2})-(\d{7})";
            //var pattern = @"\d{2}-\d{7}";

            var match = Regex.Match(tel, pattern);
            Assert.That(match.Value, Is.EqualTo("81-1234567"));
            Assert.That(match.Groups[0].Value, Is.EqualTo("81-1234567"));
            Assert.That(match.Groups[1].Value, Is.EqualTo("81"));
            Assert.That(match.Groups[2].Value, Is.EqualTo("1234567"));
        }

        [Test]
        public void GrupReferenceByNumber()
        {
            var text = "aa abbcca bbc cbc";
            var pattern = @"\b(\w)\w*\1\b";

            var matches = Regex.Matches(text, pattern);

            Assert.That(matches[0].Value, Is.EqualTo("abbcca"));
            Assert.That(matches[1].Value, Is.EqualTo("cbc"));
        }

        [Test]
        public void GroupReferenceByName()
        {
            var text = "aa aba bbc cbc";
            var pattern =
                @"\b" +
                @"(?'letter'\w)" +
                @"\w+" +
                @"\k'letter'" +
                @"\b";

            var matches = Regex.Matches(text, pattern);

            Assert.That(matches[0].Value, Is.EqualTo("aba"));
            Assert.That(matches[1].Value, Is.EqualTo("cbc"));
        }
    }

    [TestFixture]
    public class Replacement
    {
        [Test]
        public void BasicReplacement()
        {
            var text = "kot zrobił łoskot";
            var pattern = @"\bkot\b";

            var result = Regex.Replace(text, pattern, "pies");

            Assert.That(result, Is.EquivalentTo("pies zrobił łoskot")); //ups co poszło źle
        }

        [Test]
        public void MatchReference()
        {
            var text = "2 plus 2 is 4";
            var pattern = @"\d+";
            var replacement = @"<$0>";

            var result = Regex.Replace(text, pattern, replacement);

            Assert.That(result, Is.EqualTo("<2> plus <2> is <4>"));
        }
        [Test]
        public void GroupReference()
        {
            var text = "2 plus 2 is 4";
            var pattern = @"(\d+)$";
            var replacement = @"<$1>";

            var result = Regex.Replace(text, pattern, replacement);

            Assert.That(result, Is.EqualTo("2 plus 2 is <4>"));
        }

        [Test]
        public void GroupReference2()
        {
            var text = "WellTemperature";

            var result = Regex.Replace(text, "(Well)(Temperature|Pressure)", "$1Head$2");

            Assert.That(result, Is.EqualTo("WellHeadTemperature"));
            }


        [Test]
        public void MatchEvaluator()
        {
            var text = "Iza:25, Mateusz: 26, Ilona: 26, Maciek:23";
  
            var result = Regex.Replace(text, @"\d+", m =>
            {
                return (int.Parse(m.Value) <= 25)
                ? "młodszy"
                : "starszy";
            });

            Assert.That(result, Is.EqualTo("Iza:młodszy, Mateusz: starszy, Ilona: starszy, Maciek:młodszy"));
        }

        public string TestDelegate(Match m)
        {
            return "";
        }
    }

    [TestFixture]
    public class TextSplit
    {
        [Test]
        public void BasicSplit()
        {
            var text = "a1b2c3d";
            var result = Regex.Split(text, @"\d");

            Assert.That(result, Is.EqualTo(new[] { "a", "b", "c", "d" }));
        }



        [Test]
        public void SplitWithPositiveLookahead()
        {
            var text = "JedenDwaTrzy";
            var splitPattern = @"(?=[A-Z])";


            var result = Regex.Split(text, splitPattern);

            Assert.That(result, Is.EqualTo(new[] { "", "Jeden", "Dwa", "Trzy" }));
        }
    }

    [TestFixture]
    public class Recipies
    {
        [Test]
        public void RemoveDuplicatesWords()
        {
            var pattern = @"(?'d'\w+)(\W\k'd')+";
            var text = "some words occurs occurs occurs occurs more more than once";

            var result = Regex.Replace(text, pattern, @"${d}");

            Assert.That(result, Is.EqualTo("some words occurs more than once"));
        }
    }

}
