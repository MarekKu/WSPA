﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4OperatorOverloading
{
    struct Vector3D
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Z { get; set; }

        public static explicit operator Vector2D(Vector3D v)
        {
            return new Vector2D()
            {
                x = v.X,
                y = v.Y
            };
        }

        public static implicit operator Vector3D(Vector2D v)
        {
            return new Vector3D()
            {
                X = v.x,
                Y = v.y,
                Z = 0
            };
        }

        //public static implicit operator Vector3D(Vector2D v)
        //{
        //    return new Vector3D()
        //    {
        //        X = v.x,
        //        Y = v.y,
        //        Z = 0
        //    };
        //}
    }

    struct Vector2D
    {
        public Vector2D(Vector3D v)
        {
            x = v.X;
            y = v.Y;
        }
        public int x { get; set; }
        public int y { get; set; }
    }


    class ComplexNumber
    {
        public ComplexNumber()
        {
        }
        public ComplexNumber(int RealNumber, int ImaginaryNumber)
        {
            this.RealNumber = RealNumber;
            this.ImaginaryNumber = ImaginaryNumber;
        }
        public int RealNumber { get; set; }
        public int ImaginaryNumber { get; set; }
        public static int Flag { get; set; }

        public static ComplexNumber operator +(ComplexNumber a, ComplexNumber b)
        {
            var result = new ComplexNumber()
            {
                RealNumber = a.RealNumber + b.RealNumber,
                ImaginaryNumber = a.ImaginaryNumber + b.ImaginaryNumber
            };

            return result;
        }

        public static ComplexNumber operator +(ComplexNumber a, int realNumber)
        {
            a.RealNumber += realNumber;


            //var result = new ComplexNumber()
            //{
            //    RealNumber = a.RealNumber + realNumber,
            //    ImaginaryNumber = a.ImaginaryNumber
            //};

            //return result;

            return a;
        }

        public static ComplexNumber operator +(ComplexNumber a, ImaginaryNumber im)
        {
            return new ComplexNumber()
            {
                ImaginaryNumber = a.ImaginaryNumber + im.Value,
                RealNumber = a.RealNumber
            };
        }

        static Func<ComplexNumber, double> power =
            (a) => Math.Sqrt(Math.Pow(a.RealNumber, 2) + Math.Pow(a.ImaginaryNumber, 2));

        public static bool operator <(ComplexNumber a, ComplexNumber b)
        {
            return power(a) < power(b);
        }

        public static bool operator >(ComplexNumber a, ComplexNumber b)
        {
            return power(a) > power(b);
        }


        public static ComplexNumber operator |(ComplexNumber a, ComplexNumber b)
        {
            if (a)
                return a;
            else if (b)
                return b;
            return new ComplexNumber() { RealNumber = -int.MaxValue };
        }

        //public static implicit operator double(ComplexNumber x)
        //{ 
        //    return true;
        //}



    //public static ComplexNumber operator |(ComplexNumber a, ComplexNumber b)
    //{
    //    if (a)
    //        return a;
    //    else if (b)
    //        return b;
    //    return new ComplexNumber() { RealNumber = -int.MaxValue };
    //}

    public static implicit operator double(ComplexNumber x)
    {
        return Math.Sqrt(x.RealNumber * x.RealNumber + x.ImaginaryNumber * x.ImaginaryNumber);
    }

    public static explicit operator ComplexNumber(double x)
    {
        throw new Exception(); //jaka powinna być wiadomość, dlaczego nie można tak konwertować
    }

    public static bool operator true(ComplexNumber x)
    {
        Flag++;
        return x.RealNumber >= 0;
    }

    public static bool operator false(ComplexNumber x)
    {
        Flag++;
        return x.RealNumber < 0;
    }
}
}
