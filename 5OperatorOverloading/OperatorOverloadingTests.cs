﻿using NUnit.Framework;

namespace _4OperatorOverloading
{
    public struct ImaginaryNumber
    {
        private int myProperty;
        public int Value 
        {
            get
            {
                return myProperty;
            }
            set
            {
                if (value > 10)
                {
                    myProperty = 10;
                }
                else
                {
                    myProperty = value;
                }
          
            }
        }
    }

    [TestFixture]
    public class OperatorOverloadingTests
    {
        [Test]
        public void Explicit()
        {
            double d = 4.5;
            int i;

            i = (int)d;

            Assert.That(i, Is.EqualTo(4));
        }

        [Test]
        public void Implicit()
        {
            double d; 
            int i = 4;

            d = i;

            Assert.That(d, Is.EqualTo(4));
        }


        [Test]
        public void SetterTest()
        {
            var s = new ImaginaryNumber();
            s.Value = 14;

            Assert.That(s.Value, Is.EqualTo(10));
        }

        [Test]
        public void AddTwoComplexNumber()
        {
            var number1 = new ComplexNumber(1, 2);
            var number2 = new ComplexNumber(2, 3);

            var result = number1 + number2;

            Assert.That(result.RealNumber, Is.EqualTo(3));
            Assert.That(result.ImaginaryNumber, Is.EqualTo(5));
        }

        [Test]
        public void AddIntAndComplexNumber()
        {
            var number1 = new ComplexNumber()
            {
                RealNumber = 1,
                ImaginaryNumber = 2
            };

            var r = 1 + 3;

            var result = number1 + 3;

            Assert.That(result.RealNumber, Is.EqualTo(4));
            Assert.That(result.ImaginaryNumber, Is.EqualTo(2));
        }

        [Test]
        public void AddImaginaryNumberAndComplexNumber()
        {
            var number1 = new ComplexNumber()
            {
                RealNumber = 1,
                ImaginaryNumber = 2
            };

            var s = new ImaginaryNumber();
            s.Value = 6;

            var result = number1 + s;

            Assert.That(result.RealNumber, Is.EqualTo(1));
            Assert.That(result.ImaginaryNumber, Is.EqualTo(8));
        }

        [Test]
        public void Comapre()
        {
            var number1 = new ComplexNumber()
            {
                RealNumber = 1,
                ImaginaryNumber = 2
            };

            var number2= new ComplexNumber()
            {
                RealNumber = 21,
                ImaginaryNumber = 22
            }; ;

            Assert.True(number2 > number1);
        }




        [Test]
        public void AddIntToComplexNumber()
        {
            var number1 = new ComplexNumber()
            {
                RealNumber = 1,
                ImaginaryNumber = 2
            };

            number1 += 3;

            Assert.That(number1.RealNumber, Is.EqualTo(4));
            Assert.That(number1.ImaginaryNumber, Is.EqualTo(2));
        }

        [Test]
        public void ExplicitConversionVector()
        {
            var v3d = new Vector3D() { X = 1, Y = 2, Z = 3 };
            Vector2D v2d = (Vector2D)v3d;

            Assert.That(v2d.x, Is.EqualTo(1));
        }

        [Test]
        public void ImplicitConversionVector()
        {
            var v2d = new Vector2D() { x = 1, y = 2 };
            Vector3D v3d = v2d;

            Assert.That(v2d.x, Is.EqualTo(1));
        }


        [Test]
        public void ConstructorConversionVector()
        {
            var v3d = new Vector3D() { X = 1, Y = 2, Z = 3 };
            Vector2D v2d = new Vector2D(v3d);

            Assert.That(v2d.x, Is.EqualTo(1));
        }

        [Test]
        public void ImplicitConversion()
        {
            var number1 = new ComplexNumber()
            {
                RealNumber = 3,
                ImaginaryNumber = 4
            };

            double result = number1;

            Assert.That(result, Is.EqualTo(5));
        }

        [Test]
        public void ExplicitConversion()
        {
            var number = 5d;

            Assert.That(() => (ComplexNumber)number, Throws.Exception);
        }

        [Test]
        public void BoolOperatorOverloaded()
        {
            var number1 = new ComplexNumber()
            {
                RealNumber = 3,
                ImaginaryNumber = 4
            };

            var number2 = new ComplexNumber()
            {
                RealNumber = 3,
                ImaginaryNumber = 4
            };

            if (number1 || number2)
            {

            }

            Assert.That(ComplexNumber.Flag, Is.EqualTo(1)); //2
        }
    }


    public struct S
    {
        public int Value;
    }

    public class C
    {
        public int Value;
    }

    [TestFixture]
    public class SCTests
    {
        [Test]
        public void SC()
        {
            var c = new C();
            c.Value = 1;
            var s = new S();
            s.Value = 1;

            SCTests.Do(c);
            SCTests.Do(s);

            Assert.That(c.Value, Is.EqualTo(3));
            Assert.That(s.Value, Is.EqualTo(1));
        }

        public static void Do(C c)
        {
            c.Value = 3;
        }
        public static void Do(S s)
        {
            s.Value = 3;
        }
    }
}
