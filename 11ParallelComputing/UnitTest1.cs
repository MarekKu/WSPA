using System;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }


        public int CalcualteAndHandleCancelation(CancellationToken cancellationToken, int id)
        {
            for (int i = 0; i < id; i++)
            {
                cancellationToken.ThrowIfCancellationRequested();
                //Set curernt task As canceled
                //if (cancellationToken.IsCancellationRequested)
                //{
                //    return 0;
                //}
                Thread.Sleep(1000);
            }
            Debug.WriteLine("Returning: " + id);
            return id;
            //exception
        }

        public int CalcualteAndDontHandleCancelation(CancellationToken cancellationToken, int id)
        {
            Thread.Sleep(id * 1000);
            Debug.WriteLine("Returning: " + id);
            return id;
            //exception
        }

        public int CalcualteAndDontHandleCancelation(int id)
        {
            Thread.Sleep(id * 1000);
            Debug.WriteLine("Returning: " + id);
            return id;
            //exception
        }

        [Test]
        public void TaskWailAll_WithCancelationTokenHandled()
        {
            var ids = Enumerable.Range(1, 10);
            var ct = new CancellationTokenSource();
            var tasks = ids.Select(id => Task<int>.Factory.StartNew(() => CalcualteAndHandleCancelation(ct.Token, id), ct.Token)).ToArray();
            try
            {
                if (!Task.WaitAll(tasks, 5000))
                {
                    ct.Cancel();

                    // Wait for cancellation, if necessary.
                    Task.WaitAll(tasks);
                }
            }
            catch (AggregateException ae)
            {
            }

            StringBuilder result = GetResult(tasks);
            Assert.That(result.ToString(), Is.EqualTo("12345"));
        }

        [Test]
        public void TaskWailAll_WithoutCancelationTokenHandledButWaitedForIt()
        {
            var ids = Enumerable.Range(1, 10);
            var ct = new CancellationTokenSource();
            var tasks = ids.Select(id => Task<int>.Factory.StartNew(() => CalcualteAndDontHandleCancelation(ct.Token, id), ct.Token)).ToArray();
            try
            {
                if (!Task.WaitAll(tasks, 5000))
                {
                    ct.Cancel();

                    // Wait for cancellation, if necessary.
                    Task.WaitAll(tasks);
                }
            }
            catch (AggregateException ae)
            {
            }

            StringBuilder result = GetResult(tasks);

            Assert.That(result.ToString(), Is.EqualTo("12345678910"));

        }

        [Test]
        public void TaskWailAll_WithoutCancelationTokenHandledAndNoWaitingToCancelGracefully()
        {
            var ids = Enumerable.Range(1, 10);
            var ct = new CancellationTokenSource();
            var tasks = ids.Select(id => Task<int>.Factory.StartNew(() => CalcualteAndDontHandleCancelation(ct.Token, id), ct.Token)).ToArray();
            if (!Task.WaitAll(tasks, 5000))
            {
                ct.Cancel();
            }

            StringBuilder result = GetResult(tasks);

            Assert.That(result.ToString(), Is.EqualTo("1234"));
        }

        [Test]
        public void TaskWailAll_WithoutCancelationToken()
        {
            var ids = Enumerable.Range(1, 10);
            var tasks = ids.Select(id => Task<int>.Factory.StartNew(() => CalcualteAndDontHandleCancelation(id))).ToArray();
            Task.WaitAll(tasks, TimeSpan.FromSeconds(5));

            StringBuilder result = GetResult(tasks);

            Assert.That(result.ToString(), Is.EqualTo("1234"));

        }

        private static StringBuilder GetResult(Task<int>[] tasks)
        {
            var result = new StringBuilder();
            foreach (var task in tasks)
            {
                if (task.IsCompletedSuccessfully)
                {
                    result.Append(task.Result);
                }
                else
                {
                    Debug.WriteLine(task.IsCanceled);
                    Debug.WriteLine(task.Status);
                }
            }

            return result;
        }
    }
}