﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace ParallelComputing
{
    [TestFixture]
    public class AsyncAwaitTests
    {
        [Test]
        public void TestAsyncCall()
        {
            Console.WriteLine("test1");
            var result = Example.DoSthAsync();
            Console.WriteLine("test2");
            Console.WriteLine(result.Result);
            Console.WriteLine("test3");
        }

        [Test]
        public void TestCall()
        {
            Console.WriteLine("test1");
            var result = Example.DoSth();
            Console.WriteLine("test2");
            Console.WriteLine(result);
            Console.WriteLine("test3");
        }

        [Test]
        public void DoSthWrapper()
        {
            Console.WriteLine("test1");
            Example.DoSthWrapper();
            Console.WriteLine("test2");
            Console.WriteLine("test3");
            Thread.Sleep(1000);
            Console.WriteLine("test4");
        }
    }


    public class Example
    {
        public static int DoSth()
        {
            Console.WriteLine("start");
            Thread.Sleep(1000);
            Console.WriteLine("stop");
            return 3;
        }

        public static async Task<int> DoSthAsync()
        {
            Console.WriteLine("start");
            int result = 0;
            await Task.Run(() =>
            {
                result = DoSth();
            });
            Console.WriteLine("stop");
            return result;
        }

        public static async void DoSthWrapper()
        {
            Console.WriteLine("wrapperStart");
            await DoSthAsync();
            Console.WriteLine("wrapperStop");
        }
    }
}
