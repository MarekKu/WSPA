﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DataSharingAndSynchronization
{
    public class ReaderWriterLocks
    {
        // recursion is not recommended and can lead to deadlocks
        static ReaderWriterLockSlim padlock = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);

        private static IEnumerable<int> LazyYielding()
        {
            yield return 1;
            yield return 1;
            yield return 1;
            yield return 1;
        }

        static void Main(string[] args)
        {
            var list = new[] { 1, 2 }.ToList();
            //var z = list.Select(e => e);

            var z = LazyYielding().ToList();
            var a1 = z.Last();
            var iis = z.All(aaa => aaa == 1);
            var a2 = z.Last();
            var iis2 = z.All(aaa => aaa == 1);
            var c1 = z.Count;
            var c2 = z.Count;

            int x = 0;

            Task.Factory.StartNew(() =>
            {
                Console.WriteLine("zaraz zaloze writeLock");
                padlock.EnterWriteLock();
                Console.WriteLine("zaczynamy zapisywanie");
                x++;
                Thread.Sleep(3000);
                Console.WriteLine("zakonczono");
               
                padlock.ExitWriteLock();
            });
            var tasks = new List<Task>();
            for (int i = 0; i < 10; i++)
            {
                tasks.Add(Task.Factory.StartNew(() =>
                {
                    Console.WriteLine("before enterReadLock");
                    padlock.EnterReadLock();
                    //padlock.EnterReadLock();
                    //  padlock.EnterUpgradeableReadLock();

                    //Console.WriteLine("a idz z ta konwencja");
                    //                        x = 1235235;
                  
                    // can now read
                    Console.WriteLine($"Entered read lock, x = {x}, pausing for 5sec");
                    Thread.Sleep(5000);

                    padlock.ExitReadLock();
                    //padlock.ExitReadLock();
                //    padlock.ExitUpgradeableReadLock();

                    Console.WriteLine($"Exited read lock, x = {x}.");
                }));
            }

            try
            {
                Task.WaitAll(tasks.ToArray());
            }
            catch (AggregateException ae)
            {
                ae.Handle(e =>
                {
                    Console.WriteLine(e);
                    return true;
                });
            }

            Random random = new Random();

            while (true)
            {
                Console.ReadKey();
                padlock.EnterWriteLock();
                Console.WriteLine("Write lock acquired");
                int newValue = random.Next(10);
                x = newValue;
                Console.WriteLine($"Set x = {x}");
                padlock.ExitWriteLock();
                Console.WriteLine("Write lock released");
            }
        }
    }
}


//using System;
//using System.Collections.Generic;
//using System.Threading;
//using System.Threading.Tasks;

//namespace DataSharingAndSynchronization
//{
//    class MutexExample
//    {
//        static void Main(string[] args)
//        {
//            //LocalMutex();
//            GlobalMutex();

//            Console.WriteLine("All done here.");
//        }

//        private static void GlobalMutex()
//        {
//            const string appName = "MyApp";
//            Mutex mutex;
//            Mutex mutex2;
//            try
//            {
//                mutex = Mutex.OpenExisting(appName);
//                Console.WriteLine($"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!111111Sorry, {appName} is already running.");
//                return;
//            }
//            catch (WaitHandleCannotBeOpenedException e)
//            {
//                Console.WriteLine("We can run the program just fine.");
//                // first arg = whether to give current thread initial ownership
//                mutex = new Mutex(false, appName);
//                mutex2 = new Mutex(false, appName);

//                var r = object.ReferenceEquals(mutex, mutex2);
//                var m = Mutex.OpenExisting(appName);

//                Task.Factory.StartNew(() =>
//                {
//                    var can = m.WaitOne();
//                    if (can)
//                        Console.WriteLine("jestem w nowym wątku");
//                    else
//                    {
//                        Console.WriteLine("new moge");
//                    }
//                });
//                m.WaitOne();
//                m.WaitOne();
//                Console.WriteLine("mutex released");
//                Thread.Sleep(2200);
//                m.ReleaseMutex();
//                m.ReleaseMutex();

//                var obj = new object();
//                lock (obj)
//                {
//                    Console.WriteLine("before inside lock");
//                    //
//                    Task.Factory.StartNew(() =>
//                    {
//                        lock (obj)
//                        {
//                            Console.WriteLine("double lock");
//                        }
//                    });
//                    Thread.Sleep(2000);
//                    Console.WriteLine("after inside lock");
//                    //
//                }
//                Console.WriteLine("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

//                lock (obj)
//                {
//                    Console.WriteLine("before inside lock");
//                    //
//                    lock (obj)
//                    {
//                        Console.WriteLine("double lock");
//                    }
//                    Console.WriteLine("after inside lock");
//                    //
//                }


//                var r1 = object.ReferenceEquals(m, mutex);
//                var r2 = object.ReferenceEquals(m, mutex2);
//            }

//            Console.ReadKey();
//            Console.ReadKey();
//        }
//    }
//}