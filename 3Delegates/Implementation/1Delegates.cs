﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2Delegates.Implementation
{
    public delegate double[] Transformer(double[] x);

    public class MyClass
    {
        public static double[] Squere(double[] x)
        {
            var result = new double[x.Length];
            for (int i = 0; i < x.Length; i++)
            {
                result[i] = x[i] * x[i];
            }

            return result;
        }
    }



    public delegate double Converter(double x);

    class Pluginable
    {
        public static double[] Convert(double[] target, Converter func)
        {
            var result = new double[target.Length];
            for (int i = 0; i < target.Length; i++)
            {
                result[i] = func(target[i]);
            }

            return result;
        }
    }




    public delegate bool Publish(int x);  //przy multiemisji ważny jest typ zwrotny

    public class Subscriber
    {
        public int Flag { get; set; } = 0;

        public bool SetFlag(int newFlag)
        {
            var oldFlag = Flag;
            Flag = newFlag;
            return oldFlag != newFlag;
        }
    }

    public delegate void ProgressReporter(int precentComplete);

    public class Util
    {
        public static ProgressReporter p;
        public static void HardWork()
        {
            for (int i = 0; i < 10; i += 4)
            {
                p(i * 10);
            }
        }
    }



    //Generic delegates
    public delegate T ConverterGeneric<T>(T arg);

    public class UtilGeneric
    {
        public static T[] Convert<T>(T[] values, ConverterGeneric<T> c)
        {
            var result = new T[values.Length];
            for (int i = 0; i < values.Length; i++)
            {
                result[i] = c(values[i]);
            }

            return result;
        }

        public static T[] Convert2<T>(T[] values, Func<T, T> c)
        {
            var result = new T[values.Length];
            for (int i = 0; i < values.Length; i++)
            {
                result[i] = c(values[i]);
            }

            return result;
        }
    }

    class Publisher
    {
        public event Action Change = Empty;

        public static void Empty() { }

        public void MyPropertSetter(int value)
        {
            MyProperty = value;
           // Change();
        }
        public int MyProperty
        {
            set
            {
                    Change();
            }
        }
    }

}
