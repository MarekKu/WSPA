﻿using System;
using System.Collections.Generic;
using _2Delegates.Implementation;
using NUnit.Framework;

namespace _2Delegates
{
    class MyNiceClass
    {
        public int MyProperty { get; set; }
    }


    [TestFixture]
    public class DelegatesTests
    {
        public delegate double[] Transformer(double[] x);
        [Test]
        public void BasicDelegateTests()
        {
            MyNiceClass myClass;
            Transformer d;
            d = MyClass.Squere;
            double[] sampleArray;
            sampleArray = new[] { 1d, 2, 3 };

            var result = d(sampleArray);

            Assert.That(result, Is.EqualTo(new[] { 1d, 4, 9 }));
        }

        [Test]
        public void PluginTest()
        {
            Converter c = ChangeSign;
            var sampleArray = new[] { 1d, -2 };

            var result = Pluginable.Convert(sampleArray, c);

            Assert.That(result, Is.EqualTo(new[] { -1d, 2 }));
        }

        [Test]
        public void PluginTest2()
        {
            Converter c = AddOne;
            var sampleArray = new[] { 1d, -2 };

            var result = Pluginable.Convert(sampleArray, c); ;

            Assert.That(result, Is.EqualTo(new[] { 2d, -1 }));
        }

        static double ChangeSign(double x) { return -x; }
        static double AddOne(double x) { return x += 1; }

        [Test]
        public void MultiemisionTest()
        {
            var o1 = new Subscriber();
            var o2 = new Subscriber();
            o2.SetFlag(3);

            Publish c = o1.SetFlag;
            c += o2.SetFlag;  //-= , + -, null

            Delegate[] delegates = c.GetInvocationList();

            var result = c(4);

            Assert.That(o1.Flag, Is.EqualTo(4));
            Assert.That(o2.Flag, Is.EqualTo(4));
        }

        [Test]
        public void ProgressBarTest()
        {
            var reporter = new FakeReporter();
            Util.p += reporter.Report;
            Util.HardWork();

            Assert.That(reporter.result, Is.EqualTo(new[] { 0, 40, 80 }));
        }

        class FakeReporter
        {
            public List<int> result = new List<int>();

            public void Report(int percentComplete)
            {
                result.Add(percentComplete);
            }

        }



        [Test]
        public void GenericDelegate()
        {
            var sample = new[] { 1d, 2, 3 };

            var result1 = UtilGeneric.Convert(sample, AddOne);
            var result2 = UtilGeneric.Convert2(sample, AddOne);

            Assert.That(result1, Is.EqualTo(result2));
        }

        [Test]
        public void GenericDelegate2()
        {
            var sample = new[] { "a", "b", "c" };

            var result1 = UtilGeneric.Convert(sample, AddOne);
            var result2 = UtilGeneric.Convert2(sample, AddOne);

            //Assert.That(result1, Is.EqualTo(result2));
        }
        static string AddOne(string x) { return x + "1"; }




        [Test]
        public void Event()
        {
            var publisher = new Publisher();
            var subs1 = new FakeSubscriber();
            var subs2 = new FakeSubscriber();

            //publisher.Change += subs1.Notify;
            //publisher.Change += subs2.Notify;

            publisher.MyProperty = 3;

            Assert.IsTrue(subs1.WasCalled);
            Assert.IsTrue(subs2.WasCalled);
        }

        class FakeSubscriber
        {
            public bool WasCalled = false;

            public void Notify()
            {
                WasCalled = true;
            }
        }
    }
}
