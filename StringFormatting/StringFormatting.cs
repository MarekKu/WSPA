﻿using NUnit.Framework;

namespace StringFormatting
{
    [TestFixture]
    public class StringFormatting
    {
        [Test]
        public void StringFormating()
        {
            var result = string.Format("test {first} and {another}", new { another = "something else", first = "something" });

            Assert.That(result, Is.EqualTo("test something and something else"));
        }
    }
}
