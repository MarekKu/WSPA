﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3LabdaAnonymousMethodAndTypes.Implementation
{
    class TestedClass
    {
        public static Func<int> GetNumber()
        {
            int start = 0;
            return () => start++;
        }








        internal static Func<int> GetNumberInnverVariable()
        {
            return () =>
            {
                int start = 0;
                return start++;
            };
        }
    }
}
