﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using _3LabdaAnonymousMethodAndTypes.Implementation;
using NUnit.Framework;

namespace _3LabdaAnonymousMethodAndTypes
{
    [TestFixture]
    public class LambdaTests
    {

        [Test]
        public void SampleLambda()
        {
            Func<int, int> sqrt;// = MySqrt;
            sqrt = delegate (int x) { return x * x; };


            // sqrt = x => x * x;

            //sqrt = x =>
            //{
            //    return x * x;
            //};

            var result = sqrt(2);

            Assert.That(result, Is.EqualTo(4));
        }

        public static int MySqrt(int x)
        {
            return x * x;
        }

        [Test]
        public void LambdaWithManyParameters()
        {
            Func<int, int, int> multiply;
            multiply = (x, y) => x * y;

            var result = multiply(2, 3);

            Assert.That(result, Is.EqualTo(6));
        }

        [Test]
        public void LambdaWithInstructionBlock()
        {
            Func<int, int, int> max;
            max = (x, y) =>
            {
                return x > y
                ? x
                : y;
            };

            var result = max(4, 5);

            Assert.That(result, Is.EqualTo(5));
        }

        [Test]
        public void LambdaWithNoParameters()
        {
            Func<int> getRandom;
            getRandom = () => new Random().Next(0, 10);

            var result = getRandom();

            Assert.That(result, Is.GreaterThanOrEqualTo(0).And.LessThanOrEqualTo(10));
        }

        [Test]
        public void LambdaCatchMetodVariable()
        {
            decimal exchangeRate = 1.5m;
            Func<decimal, decimal> exchange = x => x * exchangeRate;
            var euro = 1;


            var dolar = exchange(euro);

            Assert.That(dolar, Is.EqualTo(1.5));
        }

        [Test]
        public void LambdaCatchMetodVariable2()
        {
            decimal exchangeRate = 1.5m;
            Func<decimal, decimal> exchange = x => x * exchangeRate;
            var euro = 1m;
            exchangeRate = 2;

            var dolar = exchange(euro);

            Assert.That(dolar, Is.EqualTo(2)); // jak się zachowa test
        }
        [Test]
        public void LambdaCatchMetodVariable3333()
        {

            var table = new[] { 1, 2, 3 }.ToList();
            var result = table.Where(x => x % 2 == 0);

            table.Add(4);

            Assert.That(result.Count, Is.EqualTo(2));
        }

        [Test]
        public void LambdaCatchMetodVariable3()
        {
            decimal exchangeRate = 1.5m;
            Expression<Func<decimal, decimal>> exchange = x => x * exchangeRate;
            var compiled = exchange.Compile();
            var euro = 1;
            exchangeRate = 2;

            var dolar = compiled(euro);

            Assert.That(dolar, Is.EqualTo(2)); // jak się zachowa test
        }



        [Test]
        public void LambdaChangeMethodVariable()
        {
            int counter = 0;
            Action callIt = () => counter++;

            callIt();
            callIt();

            Assert.That(counter, Is.EqualTo(2));
        }


        [Test]
        public void LambdaCatchClassVariable()
        {
            Action callIt = () => classCounter++;

            callIt();
            callIt();

            Assert.That(classCounter, Is.EqualTo(2));
            //co jeszcze powinien zrobić utrzymylwany test?
        }

        [Test]
        public void LambdaHoldReferenceToObjectWithCachedVariable()
        {
            int start = 0;
            var foo = TestedClass.GetNumber();

            foo();
            foo();

            Assert.That(foo(), Is.EqualTo(2));
        }

        [Test]
        public void LambdaWithInnverVariable()
        {
            var foo2 = TestedClass.GetNumberInnverVariable();

            foo2();
            foo2();

            Assert.That(foo2(), Is.EqualTo(0));  //studenci, dlaczego ten test nie przechodzi?
        }

        private int classCounter = 0;

        [Test]
        public void LambaCacheIterationCounter()
        {
            Func<int>[] foos = new Func<int>[3];
            for (int i = 0; i < 2; i++)
            {
                foos[i] = () => i;
            }

            var result = $"{foos[0]()}, {foos[1]()}";

            Assert.That(result, Is.EqualTo("0, 1"));
        }
    }
}
