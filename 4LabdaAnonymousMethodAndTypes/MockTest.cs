﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3LabdaAnonymousMethodAndTypes
{
    public interface IFake
    {
        List<string> GetList();
    }

    public class Fake : IFake
    {
        private List<string> list;

        public Fake(List<string> list)
        {
            this.list = list;
        }
        public List<string> GetList()
        {
            return list;
        }
    }

    [TestFixture]
    public class MockTest
    {
        List<string> list = new List<string>();
        private Mock<IFake> mockObject;

        [SetUp]
        public void SetUp()
        {
            mockObject = new Mock<IFake>();
            mockObject.Setup(x => x.GetList()).Returns(list);
        }

        [Test]
        public void TestMeToGetPizza()
        {
            list = new[] { "a" }.ToList();

            Assert.That(mockObject.Object.GetList().Count, Is.EqualTo(1));
        }

        [Test]
        public void TestMe2()
        {
            var fake = new Fake(list);
            list = new[] { "a" }.ToList();

            Assert.That(fake.GetList().Count, Is.EqualTo(1));
        }
    }
}
