using NUnit.Framework;
using System.Text.RegularExpressions;

namespace NUnitTestProject1
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Substring()
        {
            var input = "Urz�d Gminy Pokrzywnica";
            var sub = input.Substring(12);
            Assert.That(sub, Is.EqualTo("Pokrzywnica"));
        }

        [Test]
        public void RegexTakeLastWorld()
        {
            var input = "Urz�d Gminy Pokrzywnica";
            var pattern = @".+? (\w+)$";
            var match = Regex.Match(input, pattern);
            Assert.That(match.Groups[1].Value, Is.EqualTo("Pokrzywnica"));
        }
    }
}