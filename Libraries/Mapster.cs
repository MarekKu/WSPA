﻿using System;
using Mapster;
using NUnit.Framework;

namespace Libraries
{
    class A
    {
        public int MyProperty { get; set; }
        public DateTime MyProperty2 { get; set; }
    }

    class B
    {
        public int MyProperty { get; set; }
        public DateTime MyProperty2 { get; set; }
    }


    [TestFixture]
    public class UnitTest1
    {
        [Test]
        public void TestMethod1()
        {
            var a  = new A();

            var b = a.Adapt<B>();
        }
    }
}
