﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _9ExtensionMethods.Implementation
{
    class AmbiguousCase
    {
        public void Foo(object x) { Recorder.Record = "object"; }
    }

    static class AmbiguousCaseExtensions
    {
        public static void Foo(this AmbiguousCase t, int x) { Recorder.Record = "extension"; }
    }

    static class Recorder
    {
        public static string Record { get; set; }
    }
}
