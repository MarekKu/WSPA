﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _9ExtensionMethods.Implementation
{
    public class SomeClass
    {
        public void DoStuff()
        {
        }
        protected void DoMagic()
        {
        }
    }
    public static class SomeClassExtensions
    {
        public static void DoStuffWrapper(this SomeClass someInstance)
        {
            someInstance.DoStuff();
        }
        public static void DoMagicWrapper(this SomeClass someInstance)
        {
            //someInstance.DoMagic(); // compilation error
        }
    }
}
