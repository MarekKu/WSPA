﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace _9ExtensionMethods.Implementation
{
     public static class StringHelper
    {
        public static string Test(this A a)
        {
            return "a";
        }

        public static bool IsCapitalized(this string s)
        {
            if (string.IsNullOrEmpty(s)) return false;
            return char.IsUpper(s[0]);
        }

        public static string Pluralize(this string s)
        {
            if(s == null)
            {
                return null;
                throw new NullReferenceException();
            }
            return s + "s";
        }

        public static string Capitalize(this string s)
        {
            if(s==null)
            {
                throw new NullReferenceException();
            }
            return Regex.Replace(s, @"^[a-z]",  m => { return m.Value.ToUpper(); });
        }
    }
}
