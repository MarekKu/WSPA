﻿using System;
using System.Collections.Generic;
using static _9ExtensionMethods.Implementation.StringHelper;
using static _9ExtensionMethods.Implementation._2InterfaceExtensionMethods;
using NUnit.Framework;
using _9ExtensionMethods.Implementation;

namespace _9ExtensionMethods
{
    public class A
    {
        public string Test()
        {
            return "test";
        }
    }




    [TestFixture]
    public class ExtenstionMethodsTests
    {
        [Test]
        public void CheckStringCapitalizedWithoutExtensionMethod2()
        {
            var s1 = new A();
            var result = s1.Test();
            

            Assert.That(result, Is.EqualTo("test"));
           
        }

        [Test]
        public void CheckStringCapitalizedWithoutExtensionMethod()
        {
            var s1 = "Abc";
            var s2 = "abc";

            Assert.True(StringHelper.IsCapitalized(s1));
            Assert.False(StringHelper.IsCapitalized(s2));
        }

        [Test]
        public void CheckStringCapitalizedWithExtensionMethod()
        {
            var s1 = "Abc";
            var s2 = "abc";
            

            Assert.True(s1.IsCapitalized());
            Assert.False(s2.IsCapitalized());
        }

        [Test]
        public void StringCollectionExtensionMethod()
        {
            var cs = new CandyService();
            var candy = cs.GetNext().CheckIfAlergic();



            IEnumerable<string> sc = new[] { "a", "b" };

            var result = sc.First();

            Assert.That(result, Is.EqualTo("a"));
        }

        [Test]
        public void GenericCollectionExtensionMethod()
        {
            string s = null;
            var result = s.Pluralize();

            Assert.That(result, Is.EqualTo("s"));
        }

        [Test]
        public void ExtensionMethodChain()
        {
            var s = "abc";

            var result = s
                .Pluralize()
                .Capitalize();

            Assert.That(result, Is.EqualTo("Abcs"));
        }

        [Test]
        public void Test()
        {
            bool yesNoBool = YesNo.Yes.ToBool();
            Assert.True(yesNoBool);

            YesNo yesNoEnum = false.ToYesNo();
            Assert.That(yesNoEnum, Is.EqualTo(YesNo.No));

            bool? b = null;
            var yn = b.ToYesNo();
            Assert.That(yn, Is.EqualTo(YesNo.No));
        }

        [Test]
        public void ExtensionMethodBindingTest()
        {
            Derived derived = new Derived();
            Base @base = derived;

            Assert.That(derived.GetName(), Is.EqualTo("Derived"));
            Assert.That(@base.GetName(), Is.EqualTo("Derived"));

            Assert.That(derived.GetNameByExtension(), Is.EqualTo("Derived"));
            Assert.That(@base.GetNameByExtension(), Is.EqualTo("Base"));
        }

        [Test]
        public void AmbiguousCase()
        {
            var o = new AmbiguousCase();
            o.Foo(1);
            
            Assert.That(Recorder.Record, Is.EqualTo("object"));
        }

        [Test]
        public void Jigsaw()
        {
            var dictList = new Dictionary<string, List<int>>();
          

            dictList.Add("example", 10);
            dictList.Add("example", 15);

            Assert.That(dictList["example"], Is.EqualTo(new[] { 10, 15 }));
        }


        [Test]
        public void Dictionary()
        {
            var dictList = new Dictionary<string, List<int>>();

            Func<List<int>> action = () => new List<int>();


            dictList
                .ComputeIfAbsent("example", action)
                .Add(5);

            //dictList.Add("example", 5, () => new List<int>());
            dictList.Add("example", 10);
            dictList.Add("example", 15);
            Assert.That(dictList["example"], Is.EqualTo(new[] { 5, 10, 15 }));


            //dictList.Remove("example", 5);
            //dictList.Remove("example", 10);
            //Assert.That(dictList["example"], Is.EqualTo(new[] {15 }));
            //dictList.Remove("example", 15);
            //Assert.False(dictList.ContainsKey("example"));
        }

        [Test]
        public void SealeadExtentionMethod()
        {
            var obj = new SealeadMethod();

            Assert.That(obj.Test(), Is.EqualTo("a"));
        }
    }
}
