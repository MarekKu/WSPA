﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Generics
{
    [TestFixture]
    class CovarianceTests
    {
        Stack<Bear> bears = new Stack<Bear>();
        Stack<Animal> animals = new Stack<Animal>();

        [Test]
        public void StackTest()
        {
            //animals = bears; //błąd kompilacji
            animals.Push(new Camel());   //gdy by linijka wyżej sie skompilowała, możnaby dodać wielbłądy do niedźwiedzi. 

         //   Stack<Bear> a = animals;
            //bears.Push(new Camel());//błąd kompilacji

        }

        [Test]
        public void ZooCleanerTestFirstVersion()
        {
            ZooCleaner.Wash(bears);
            ZooCleaner.Wash(new Stack<Camel>());
        }

        [Test]
        public void ZooCleanerTestSecondVersion()
        {
            var bears2 = new PopableStack<Bear>();
            var animals2 = new PopableStack<Animal>();
            //animals2 = bears2;
            //bears2.Push(new Camel());

            bears2.Push(new Bear() { Name = "Stefan" });
            IPopable<Animal> cleanerInput = (IPopable<Animal>)bears2;

            var wahsedAnimals = ZooCleaner.Wash2(bears2);

            Assert.That(wahsedAnimals, Is.EqualTo(new[] { "Stefan" }));
            // ZooCleaner.Wash(bears);
        }
    }





    class ContravarianceTests
    {
        Stack<Bear> bears = new Stack<Bear>();
        Stack<Animal> animals = new Stack<Animal>();

        [Test]
        public void StackTest()
        {
            //bears = animals; //błąd kompilacji

        }
    }

    interface A
    {
        void AM();
   }

    interface B
    {
        void Bm();
    }

    class AB : A, B
    {
        public void AM()
        {
            throw new NotImplementedException();
        }

        public void Bm()
        {
            throw new NotImplementedException();
        }
    }
}
