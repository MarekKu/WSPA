﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenericsTests.Implementation;
using GenericsTests.Implementation.GenericTypeLimitation;

namespace GenericsTests.Tests
{
    class GenericTypeConstraint
    {
        public void GivenBaseClassLimitedGeneric_WhenCreating_CreatesOnlyWithProperT()
        {
            var sample1 = new BasicClass<NewA>();
            var sample2 = new BasicClass<B>();
            var sample3 = new BasicClass<C>();
        }

        public void ParameterlessConstructorLimitation()
        {
            var sample1 = new ParemeterLessConstructor<WithParameterlessConstructor>();
            //var sample2 = new ParemeterLessConstructor<WithoutParameterlessConstructor>();
        }

        public void NakedType()
        {
            var example = new NakedTypeLimitation<BaseClass>();
            example.Add(new Class1());
            //example.Add2(new Class1());

        }

        public class BaseClass { }
        public class Class1 : BaseClass { }
    }
}
