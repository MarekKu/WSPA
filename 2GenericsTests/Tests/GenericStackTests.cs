﻿using System;
using GenericsTests.Implementation;
using NUnit.Framework;

namespace GenericsTests
{
    [TestFixture]
    public class GenericStackTests
    {
        [Test]
        public void CanAddIntToGenericStack()
        {
            var intStack = new IntegerStack();
            intStack.Push(4);

            var stringStack = new StringStack();
            stringStack.Push("aaaa");

            //100 typów dodatkowo


            var genericStack = new GenericStack<string>();

           // genericStack.Push(1);
            genericStack.Push("1");

            var result = genericStack.Pop();

            //Assert.That(result, Is.EqualTo(1));
            //Assert.That(genericStack.GetUsedType(), Is.EqualTo("System.Int32"));
            //Assert.That(genericStack.GetType().ToString(),
            //    Is.EqualTo(
            //        "GenericsTests.Implementation.GenericStack`1[System.Int32]"));
            //Assert.That(typeof(GenericStack<int>).ToString(),
            //    Is.EqualTo(
            //        "GenericsTests.Implementation.GenericStack`1[System.Int32]"));
        }

        [Test]
        public void CanAddIntAndStringToObjectStack()
        {
            var objectStack = new ObjectStack();

            objectStack.Push(1);
            objectStack.Push("1");

            var result = objectStack.Pop();

            Assert.That(result, Is.EqualTo("1"));
        }

        [Test]
        public void InferingType()
        {
            var a = 3;
            var b = 4;
       

            MyUtils.Swap(ref a, ref b);

            Assert.That(a, Is.EqualTo(4));
            Assert.That(b, Is.EqualTo(3));
        }

     

        [Test]
        public void Multiple()
        {
            var tested = new ParametersTypeDeclared<int>();
            var tested2 = new ParametersTypeDeclared<int, string>();
        }

        [Test]
        public void T()
        {
            var a = new TestStatic();
            var b = new TestStatic();
            a.E = 2;
            a.SetStatic(3);

            Assert.That(b.E, Is.EqualTo(0));
            Assert.That(b.GetStatic(), Is.EqualTo(3));
        }
    }
    class TestStatic
    {
        public static int S;
        public int E;
        public int GetStatic()
        {
            return S;
        }
        public void SetStatic(int s)
        {
            S = s;
        }
    }
}
