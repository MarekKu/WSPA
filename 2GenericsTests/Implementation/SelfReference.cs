﻿using System;

namespace GenericsTests.Implementation
{
    public interface IEquatable<T>
    {
        bool Equals(T obj);
    }

    class Car<T> : IEquatable<T> // co jest złego w tej klasie? 
    {
        public bool Equals(T obj)
        {
            throw new NotImplementedException();
        }
    }










    class Table : IEquatable<Table> 
    {
        public double Width { get; set; }
        public double Length { get; set; }
        public bool Equals(Table obj)
        {
            return this.Width == obj.Width && this.Length == obj.Length;
        }
    }

 

}
