﻿using System;
using System.Collections.Generic;

namespace GenericsTests.Implementation
{
    class MyUtils
    {
        public static void Swap<T>(ref T a, ref T b)
        {

            T temp = a;
            a = b;
            b = temp;
        }

        public static void NoGenericMethod(B b)
        {
        }

     
    }
}
