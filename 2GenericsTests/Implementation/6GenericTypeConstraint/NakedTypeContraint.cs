﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericsTests.Implementation.GenericTypeLimitation
{
    class NakedTypeLimitation<T>
    {
        List<T> list = new List<T>();
        public void Add<U>(U param) where U : T   // wymusza dziedziczenie
        {
            list.Add(param);
        }

        //public void Add2<U>(U param)
        //{
        //    list.Add(param); //IsConversionPossible
        //}
    }
}
