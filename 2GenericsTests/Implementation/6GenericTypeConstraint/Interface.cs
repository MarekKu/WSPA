﻿using System;

namespace GenericsTests.Implementation.GenericTypeLimitation
{
    interface IComparable<T>
    {
        int CompareTo(T other);
    }

    class Utils
    {
        public static T Max<T>(T a, T b) where T : IComparable<T>
        {
            //null test ommited
            return a.CompareTo(b) > 0 ? a : b;
        }
    }

    class TestA : IComparable
    {
        public int CompareTo(object obj)
        {
            throw new NotImplementedException();
        }
    }
}
