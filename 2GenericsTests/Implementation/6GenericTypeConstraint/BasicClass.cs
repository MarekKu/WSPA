﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericsTests.Implementation
{
    class BasicClass<T> where T : NewA
    {
        public void A(T a)
        {
            a.MyProperty = 5;
            a.MyAProperty = 4;
        }
    }

    class BasicClass 
    {
        public void A(NewA a)
        {
            a.MyProperty = 5;
            a.MyAProperty = 34;
        }
    }
    class NewA
    {
        public int MyProperty { get; set; }
        public int MyAProperty { get; set; }
    }
    class B : NewA
    {
        public int MyBProperty { get; set; }
    }
    class C : NewA
    {
        public int MyCProperty { get; set; }
    }
}
