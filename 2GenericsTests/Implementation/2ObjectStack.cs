﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericsTests.Implementation
{
    class ObjectStack
    {
        int position;
        object[] data = new object[100];
        public void Push(object obj)
        {
            data[position++] = obj;
        }

        public object Pop()
        {
            return data[--position];
        }
    }
}
