﻿namespace GenericsTests.Implementation
{
        class GenericStack<T>
    {
        int position;
        T[] data = new T[100];
        public void Push(T obj)
        {
            data[position++] = obj;
        }

        public T Pop()
        {
            return data[--position];
        }


        public string GetUsedType()
        {
            return typeof(T).ToString();
        }
    }

    class StringStack
    {
        int position;
        string[] data = new string[100];
        public void Push(string obj)
        {
            data[position++] = obj;
        }

        public string Pop()
        {
            return data[--position];
        }


        public string GetUsedType()
        {
            return typeof(int).ToString();
        }
    }

    class IntegerStack
    {
        int position;
        int[] data = new int[100];
        public void Push(int obj)
        {
            data[position++] = obj;
        }

        public int Pop()
        {
            return data[--position];
        }


        public string GetUsedType()
        {
            return typeof(int).ToString();
        }
    }
}
