﻿namespace GenericsTests.Implementation
{
    class StaticDataOwner<T>
    {
        public static int StaticProperty { get; set; } = 0;

        public T MyProperty { get; set; }
    }
}
