﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericsTests.Implementation
{
    class GenericArrayWithDefaulyValuesBuilder
    {
        public static T[] Build<T>(int size)
        {
            var array = new T[size];

            for(int i=0; i<array.Length; i++)
            {
                array[i] = default(T);
            }

            return array;
        }
    }
}
