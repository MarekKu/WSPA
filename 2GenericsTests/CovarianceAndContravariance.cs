﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generics
{
    class Animal
    {
        public string Name { get; set; }
    }
    class Bear : Animal { }
    class Camel : Animal { }





    class ZooCleaner
    {
        public static List<string> Wash<T>(Stack<T> animals) where T : Animal// co zrobić, zeby test ZooCleanerTests przechodził
        {
            var result = new List<string>();

            foreach (var animal in animals)
            {
                result.Add(animal.Name);
            }

            return result;
        }

        internal static List<string> Wash2(IPopable<Animal> animals)
        {
            var result = new List<string>();

            try
            {
                result.Add(animals.Pop().Name);
            }
            catch (IndexOutOfRangeException) { }

            return result;
        }
    }

    interface IPopable<out T>
    {
        T Pop();
        //  void Push(T param);  //Invalid variance
    }


    class PopableStack<T> : IPopable<T>
    {
        int position;
        T[] data = new T[100];
        public void Push(T obj)
        {
            data[position++] = obj;
        }

        public T Pop()
        {
            return data[--position];
        }

        public string GetUsedType()
        {
            return typeof(T).ToString();
        }
    }

    class Contravariance<T> : IPopable<T>
    {
        private T test;

        public T Pop()
        {
            throw new NotImplementedException();
        }
    }

    interface IPushable<in T>
    {
        //   T MyProperty { set; }
        //T Pop();  //Invalid variance
        void Push(T param);
    }




    public class ActionArgument
    {
        internal static void CallWithPrinter(Action<Action<Animal>> animalGenerator)
        {
        }

        internal static void ExpectAnimal(Action<Animal> animalGenerator)
        {
        }

        internal static void ExpectBear(Action<Bear> animalGenerator)
        {
        }
    }

    public class ActionArgumentCalles
    {
        internal void TakeBear(Bear b) {
        }

        internal void TakeAnimal(Animal b)
        {
        }

        public void TestIt()
        {
            ActionArgument.ExpectAnimal(TakeAnimal);
            //ActionArgument.CallWithPrinter2(TakeBear);

            ActionArgument.ExpectBear(TakeAnimal);
            ActionArgument.ExpectBear(TakeBear);
        }
    }
}
