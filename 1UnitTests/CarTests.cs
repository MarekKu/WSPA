﻿using NUnit.Framework;
using Rhino.Mocks;

namespace UnitTests
{
    [TestFixture]
    public class CarBasicTests
    {
        [SetUp]
        public void SetUp()
        {
            car = new Car(new CivicEngine(), 60);
        }

        [Test]
        public void GivenFullGasTank_WhenFueling_CarHasFuel()
        {
            car.Fuel(10, gasStation);
            var gasInTankAfter = car.FuelLeft;

            Assert.That(gasInTankAfter, Is.EqualTo(10));
        }


        [Test]
        public void GivenFuel_WhenCalculationRange_ReturnRange()
        {
            car.Fuel(40, gasStation);

            var result = car.Range();

            Assert.That(result, Is.EqualTo(500));
        }

        [Test]
        public void GivenGasStation_WhenFueling_ThenCallsGasStationGetFuelMethod()
        {
            var ourAmout = 30;
            car.Fuel(ourAmout, gasStation);

            Assert.That(gasStation.LastFueling, Is.EqualTo(ourAmout));
        }


        private GasStation gasStation = new GasStation();
        private Car car;
    }

    [TestFixture]
    public class CarEngineDepentencyTest
    {
        private Car car;

        [SetUp]
        public void SetUp()
        {

        }

        [Test]
        public void GivenACar_WhenRiding_FuelIsConsumed()
        {
            GivenAnEngineConsuming4L();

            WhenRiging();

            Assert.That(car.FuelLeft, Is.EqualTo(6));
        }

        private void WhenRiging()
        {
            car.Drive(50);
        }

        private void GivenAnEngineConsuming4L()
        {
            car = new Car(new FakeEngine(8), 60);
        }

        [Test]
        public void GivenACar_WhenRiding_FuelIsConsumed2()
        {
            var fakeGasStation = MockRepository.GenerateStub<GasStation>();
            var fuelConsumedByEngine = 4;
            var engine = GivenAnEngineConsuming(fuelConsumedByEngine);
            car = new Car(engine, 60);
            car.Fuel(10, fakeGasStation);

            WhenRiging();

            Assert.That(car.FuelLeft, Is.EqualTo(6));
        }

        private static IEngine GivenAnEngineConsuming(int fuelConsumedByEngine)
        {
            var engine = MockRepository.GenerateStub<IEngine>();
            engine.Stub(x => x.Ride(Arg<int>.Is.Anything))
                .Return(fuelConsumedByEngine);

            return engine;
        }

        class FakeEngine : IEngine
        {
            public FakeEngine(int fuelConsumption)
            {
                FuelConsumption = fuelConsumption;
            }

            public int FuelConsumption { get; private set; }
            public double Ride(double range)
            {
                return 4;
            }
        }
    }
}
