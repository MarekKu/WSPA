﻿using System;

namespace UnitTests
{
    public class Car
    {
        public Car(IEngine engine, double gasTankCapacity)
        {
            this.engine = engine;
            this.GasTankCapacity = gasTankCapacity;
        }
        public double FuelLeft { get; private set; }

        public void Fuel(double gas, GasStation station)
        {
            FuelLeft += station.GetFuel(gas);
        }

        public double Range()
        {
            return FuelLeft / engine.FuelConsumption * 100;
        }

        public void Drive(int range)
        {
            var fuelConsumed = engine.Ride(range);

            FuelLeft -= fuelConsumed;
        }

        public readonly double GasTankCapacity;
        private readonly IEngine engine;
    }

    public class GasStation
    {
        public double LastFueling;
      
        public double GetFuel(double fuel)
        {
            LastFueling = fuel;

            return fuel;
        }

    }

    public interface ITill
    {
        void GasFueled(double fuel);
    }

    public class Till : ITill
    {
        public void GasFueled(double fuel)
        {

        }
    }

    public interface IEngine
    {
        int FuelConsumption { get; }
        double Ride(double range);
    }

    public class CivicEngine : IEngine
    {
        public int FuelConsumption
        {
            get
            {
                if (true)
                    return 8;
                return 5;
            }
        }
        public double Ride(double range)
        {
            return range / 100 * FuelConsumption;
        }
    }
}
