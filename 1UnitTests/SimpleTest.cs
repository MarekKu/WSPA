﻿using NUnit.Framework;

namespace UnitTests
{
    [TestFixture]
    public class SimpleTest
    {
        [Test]
        public void GivenACalculator_WhenAdding_ReturnSumOfPassedNumber()
        {
            var firstNumber = 1;
            var secondNumber = 2;
            var expectedResult = 3;
            var calc = GivenACalculator();

            var result = calc.Add(firstNumber, secondNumber);

            Assert.That(result, Is.EqualTo(expectedResult));
        }

        private static Calculator GivenACalculator()
        {
            return new Calculator();
        }
    }

    public class Calculator 
    {
        public int Add(int a, int b)
        {
            return a+b;
        }
    }
}
