﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ.CheckPerformance
{
    public class Element
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    [TestFixture]
    public class TestComparer
    {
        [SetUp]
        public void SetUp()
        {
            elements = Enumerable.Range(0, 1000000).Select(x => new Element { Id = x, Name = (x % 100).ToString() });
        }

        [Test]
        public void TestTime()
        {
            var t1  = TestIt(FindDuplicateML.FindOriginal);
            var t2  = TestIt(FindDuplicateML.Find);
        }

        public long TestIt(Func<IEnumerable<Element>, string[]> function)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            function(elements);
            sw.Stop();

            return sw.ElapsedMilliseconds;
        }

        private IEnumerable<Element> elements;
    }


    static class FindDuplicateML
    {
        public static string[] FindOriginal(IEnumerable<Element> elements)
        {
            return elements.GroupBy(e => e.Id)
                .Select(g => new { Element = g.First(), Count = g.Count() })
                .Where(x => x.Count > 1)
                .Select(x => x.Element.Name).ToArray();
        }

        public static string[] Find(IEnumerable<Element> elements)
        {
            return elements.GroupBy(x => x.Id).Where(x => x.Count() > 1).Select(g => g.First().Name).ToArray();
        }
    }
}
