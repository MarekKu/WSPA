﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace LINQ
{
    public class AB //: IEquatable<AB>
    {
        public int MyProperty { get; set; }

        //public bool Equals(AB other)
        //{
        //    return this.MyProperty == other.MyProperty;
        //}
    }


    public class ABEqualityComparer : IEqualityComparer<AB>
    {
        public bool Equals(AB x, AB y)
        {
            return x.MyProperty == y.MyProperty;
        }

        public int GetHashCode(AB obj)
        {
            return obj.MyProperty.GetHashCode();
        }
    }

    [TestFixture]
    public class InjectEqualityComparerIntoOperators
    {
        [Test]
        public void GivenObjectAndNullInCollection_WhenExcept_ThrowsNullreference()
        {
            var col = new[]
            {
                new AB() {MyProperty = 1},
                new AB() {MyProperty = 2},
                new AB() {MyProperty = 3},
                new AB() {MyProperty = 4},
               // null
            };

            var col2 = new[]
 {
                new AB() {MyProperty = 1}
              //  null
            };

            var result = col.Except(col2, new ABEqualityComparer());

            Assert.That(result.Count, Is.EqualTo(1));
        }

        [Test]
        public void GivenObjectInDictionary_ChangeValues_ThenItRefersToOther()
        {
            var dict = new Dictionary<AB, string>(new ABEqualityComparer());
            var a = new AB() { MyProperty = 1 };
            var b = new AB() { MyProperty = 2 };

            dict[a] = "a";
            dict[b] = "b";

            a.MyProperty = 2;
           // b.MyProperty = 1;

            var result = dict[a];

            Assert.That(result, Is.EqualTo("b"));

        }
    }

    //In first order GetHashCode is checked, if it equal then Equals is called
}
