﻿using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace LINQ
{
    public class UnionItem
    {
        public int MyProperty { get; set; }
    }

    public class UnionItemComparer : IEqualityComparer<UnionItem>
    {
        public bool Equals(UnionItem x, UnionItem y)
        {
            return x.MyProperty == y.MyProperty;   
        }

        public int GetHashCode(UnionItem obj)
        {
            return obj.MyProperty;
        }
    }

    [TestFixture]
    public class UnionTests
    {
        [Test]
        public void UnionReferenceType()
        {
            UnionItem[] firstSet, secondSet;
            GivenSets(out firstSet, out secondSet);

            var result = firstSet.Union(secondSet);

            Assert.That(result.Count, Is.EqualTo(4));
        }

        [Test]
        public void UnionReferenceType2()
        {
            UnionItem[] firstSet, secondSet;
            GivenSets(out firstSet, out secondSet);

            var result = firstSet.Union(secondSet, new UnionItemComparer());

            Assert.That(result.Count, Is.EqualTo(3));
        }

        private static void GivenSets(out UnionItem[] firstSet, out UnionItem[] secondSet)
        {
            firstSet = new[]
            {
                new UnionItem{MyProperty = 1},
                new UnionItem{MyProperty = 2 },
            };
            secondSet = new[]
{
                new UnionItem{MyProperty = 1},
                new UnionItem{MyProperty = 3 },
            };
        }
    }
}
