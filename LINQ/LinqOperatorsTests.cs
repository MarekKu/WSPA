﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using NUnit.Framework;
using System.IO;
using System.Text.RegularExpressions;

namespace LINQ
{
    class machine
    {
        private bool A;

        public int m1()
        {
            if (A)
                return 1;
            else
                return 2;
        }

        public void setA(bool a)
        {
            A = a;
        }   
}





    [TestFixture]
    class SelectManyTests
    {
        [TestCase(TestName ="Name1",Description ="hah1")]
        [TestCase(TestName ="Name2", Description ="ba")]
        public void SthElse()
        {
            Assert.Fail();
        }



        [Test]
        public void SelectManyTest()
        {
            var number = new[] { 1, 2, 3 };
            var number2 = new[] { 1, 2, 3 };

            var result = number.SelectMany(y => number2.Select(x => y+ x));

            Assert.That(result.Count, Is.EqualTo(9));
        }
    }

    [TestFixture]
    class JoiningTests
    {
        [Test]
        public void JoinTest()
        {
            var a = new[] { 1, 2, 3 };
            var b = new[] { new { X = 1, Y = 2 }, new { X = 11, Y = 22 }, new { X = 11, Y = 22 }, new { X = 1, Y = 2 } };

            var result = a.Join(b, x => x, y => y.X, (x, y) => new { x, y });

            Assert.That(result, Is.EqualTo(new[] {
                new { x = 1, y = new { X = 1, Y = 2 } },
                new { x = 1, y = new { X = 1, Y = 2 } }
            }));
        }

        [Test]
        public void GroupJoinTest()
        {
            var a = new[] { 1, 2, 3 };
            var b = new[] { new { X = 1, Y = 2 }, new { X = 11, Y = 22 }, new { X = 11, Y = 22 }, new { X = 1, Y = 2 } };

            var result = a.GroupJoin(b, x => x, y => y.X, (x, y) => new { x, y });

            Assert.That(result.First().y, Is.EqualTo(
                new[] { new { X = 1, Y = 2 }, new { X = 1, Y = 2 } } ));

            //Assert.That(result.First(), Is.EqualTo(new[] {
            //    new { x = 1, y = new[] { new { X = 1, Y = 2 }, new { X = 1, Y = 2 } } }
            //}));
        }
    }

    [TestFixture]
    public class OrderingTests
    {
        [Test]
        public void OrderingTest()
        {
            var input = new[] {
                new { X = 1, Y = 2 },
                new { X = 2, Y = 2 },
                new { X = 2, Y = 1 },
                new { X = 1, Y = 1 } };

            var expectedResult = new[] {
                new { X = 1, Y = 1 },
                new { X = 1, Y = 2 },
                new { X = 2, Y = 1 },
                new { X = 2, Y = 2 } };

            var result = input.OrderBy(e => e.X).ThenBy(e => e.Y);

            Assert.That(result, Is.EqualTo(expectedResult));
        }
    }

    [TestFixture]
    public class GroupingByTest
    {
        [Test]
        public void DirctoryTest()
        {
            var files = Directory.GetFiles(@"C:\Users\kusmirekm\Documents\Visual Studio 2015\Projects\WSPA\8ExtensionMethods\bin\Debug");
            //var result = files.GroupBy(f => f.Split('.').Last());
            //var result = files.GroupBy(f => Regex.Match(f, @"\..+?$", RegexOptions.RightToLeft).Value);
            var result = files.GroupBy(f => Regex.Match(f, @"\.\w+?$").Value);
        }


        [Test]
        public void GroupByTest()
        {
            var input = new[] {
                new { X = 1, Y = 2 },
                new { X = 2, Y = 2 },
                new { X = 2, Y = 1 },
                new { X = 1, Y = 1 } };

            var expectedResult = new[] {
                new[] {new { X = 1, Y = 2 }, new { X = 1, Y = 1 }  },
                new[] {new { X = 2, Y = 2 }, new { X = 2, Y = 1 }  }};

            var result = input.GroupBy(x => x.X + x.Y);

            Assert.That(result, Is.EqualTo(expectedResult));
        }

        [Test]
        public void ToDictionaryTest()
        {
            var input = new[] {
                new { X = 1, Y = 2 },
                new { X = 2, Y = 2 },
                new { X = 2, Y = 1 },
                new { X = 1, Y = 1 } };

            var result = input.ToDictionary(x => x.X + x.Y);
        }
    }

    public class A { }
    public class B : A { }



    [TestFixture]
    public class ConversionTest
    {
        [Test]
        public void CastTests()
        {
            var array = new object[] { 1, null };

            Assert.That(array.OfType<int>().ToArray(), Is.EqualTo(new[] { 1 }));
            Assert.That(() => array.Cast<int>().ToArray(), Throws.Exception.TypeOf<NullReferenceException>());
        }
    }

    [TestFixture]
    public class AggregationTest
    {
        [Test]
        public void AggregationsTest()
        {
            var input = new[] { 1, 2, 3 };
            //1+4+9 = 14

            //var result = input.Aggregate(0, (total, n) => total * n);
            var result = input.Aggregate(0, (total, n) => total + n * n);

            Assert.That(result, Is.EqualTo(14));
        }
    }


    [TestFixture]
    public class QuantifierTest
    {
        [Test]
        public void SequenceEqualTest()
        {
            var a = new[] { 2, 1, 3 };
            var b = new[] { 1, 2, 3 };

            

            Assert.True(a.OrderBy(x => x).SequenceEqual(b.OrderBy(x=> x)));
        }
    }
}
