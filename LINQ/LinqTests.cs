﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using NUnit.Framework;

namespace LINQ
{
    [TestFixture]
    public class NewLinqTests
    {
        [Test]
        public void FirstSelect()
        {
            var coll = new[] { "aa", "bbb", "c" };

            var result = coll.Where(x => x.Length == 2);
            



        }

        [Test]
        public void Ex14()
        {
            var grades = new[]
            {
                new {Name = "Jan1", Grade = 1 },
                new {Name = "Jan4", Grade = 4 },
                new {Name = "Jan2", Grade = 2 },
                new {Name = "Jan3", Grade = 3 },
                new {Name = "Jan33", Grade = 3 },
                new {Name = "Jan5", Grade = 5 },
                new {Name = "Jan55", Grade = 5 },
            };
            var n = 3;
            var grade = grades.OrderByDescending(x => x.Grade).Select(x => x.Grade).Distinct().ElementAt(n - 1);

            var res = grades.Where(x => x.Grade == grade);

            Assert.That(res, Is.EqualTo(new[] { new { Name = "Jan3", Grade = 3 }, new { Name = "Jan33", Grade = 3 } }));
        }

        [Test]
        public void Ex14v2()
        {
            var grades = new[]
            {
                new {Name = "Jan1", Grade = 1 },
                new {Name = "Jan4", Grade = 4 },
            };
            var n = 3;
            var grade = grades.OrderByDescending(x => x.Grade).Select(x => x.Grade).Distinct().ElementAtOrDefault(n - 1);

            var res = grades.Where(x => x.Grade == grade);

            Assert.That(res, Is.Empty);
        }

        [Test]
        public void Ex15()
        {
            //1 File(s) with .frx Extension 
            var files = Directory.GetFiles(@"C:\Users\kusmirekm\Documents\Visual Studio 2015\Projects\WSPA\8ExtensionMethods\bin\Debug").ToList();

            files.Add("mandzaro");
            files.Add(".mandzaro");
            files.Add("mandzaro.");
            files.Add("mandzaro.empty");
           
            var result = files.GroupBy(f => {
                var s = f.Split('.');
                return s.Count() > 1
                ? s.Last()
                : string.Empty;
                });
            Func<IGrouping<string, string>, string> ext = (x) =>  (x.Key != string.Empty) ? (" " + x.Key) : "out";
            var outt = result.Select(x => $"{x.Count()} with{ext(x)} ext");
            var r = string.Join(Environment.NewLine, outt);

        }

        [Test]
        public void Ex16()
        {
            var files = Directory.GetFiles(@"C:\Users\kusmirekm\Documents\Visual Studio 2015\Projects\WSPA\8ExtensionMethods\bin\Debug");
            var result = files.Select(f => new FileInfo(f).Length).Average();

            //var fiel = new FileInfo(files[1]);
            //fiel.Length;
        }

        [Test]
        public void Ex18()
        {
            var l = Enumerable.Range(1, 10);
            var toDelete = 5;

            var result = l.Where(x => x!= toDelete);

            Assert.That(result, Has.No.Member(toDelete));
            
        }

        [Test]
        public void Ex21()
        {
            var l = new[] { 1, 2, 3, 4, 5, 6, 6, 7, 8, 9, 10 }; 
            var startIndex = 3;
            var endIndex = 6;

            var r = l.Where((x, i) => i < startIndex || i >= endIndex);

            Assert.That(r, Is.EqualTo(new[] { 1, 2, 3, 6, 7, 8, 9, 10}));
        }

        [Test]
        public void Ex22()
        {
            var l = new[] { "a", "ccc", "bb", "dddd" };
            var minChar = 2;

            var result = l.Where(x => x.Length <= minChar);
        }

        [Test]
        public void Ex23()
        {
            var d = new[] { "1", "2", "3" };
            var l = new[] { "a", "b", "c" };

            var result = l.SelectMany(x => d.Select(y => x + y));

        }

        [Test]
        public void Ex24()
        {
            var d = new[] { "1", "2", "3" };
            var l = new[] { "a", "b", "c" };
            var s = new[] { "!", "@", "#" };

            var result = l.SelectMany(x => d.SelectMany(y => s.Select(z => x+y+z)));

        }

        [Test]
        public void Ex25()
        {
            var items = new[] {
                  new {Id =1, Name = "n1"},
                  new {Id =2, Name = "n2"},
                  new {Id =3, Name = "n3"},
            };
            var q = new[] {
                new {Id =1, ItemId =1, PurchaseQuantity  = 111},
                new {Id =3, ItemId =3, PurchaseQuantity  = 222},
            };

            var result = items.Join(q, x => x.Id, y => y.ItemId, (x, y) => $"{x.Id}, {x.Name}, {y.PurchaseQuantity}");

        }

        [Test]
        public void FotosSegregation()
        {
            var directory = @"D:\test";
            var files = Directory
                .GetFiles(directory, "*", SearchOption.AllDirectories)
                .Distinct()
                .Select(f => new FileInfo(f));

            files.Select(f => Utils.ProcesFile(f));

            //var f = new FileInfo("ddd");
            //f.Cr

            //File.Move()

        }

        [Test]
        public void GivenAFileInfo_WhenCreatingNewFileName_ReturnsNameAndDirBasedOnCreationDate()
        {
            var testFileName = "test";
            var testCreationDate = new DateTime(2011, 2, 3, 4, 5, 6);
            var testFile = new FileInfo(testFileName);
            testFile.CreationTime = testCreationDate;

            var result = Utils.GetDirAndName(testFile);

            Assert.That(result, Is.EqualTo(@"2011\2\3\4:5:6\"+testFileName));
        }


        public string AddX(string input)
        {
            return input + "x";
        }
    }


    class Utils
    {
        public static FileInfo ProcesFile(FileInfo f)
        {
            return null;
        }

        internal static string GetDirAndName(FileInfo f)
        {
            return f.CreationTime.ToString("yyyy\\MM\\dd\\HH:mm:ss") + f.Name;
        }
    }























    [TestFixture]
    public class LinqTests
    {
        [Test]
        public void FirstSelect()
        {
            var coll = new[] { "a", "b", "c" };

            var result = coll.Select(x => x + "x");
        }

        [Test]
        public void SecondSelect()
        {
            var coll = new[] { "a", "bb", "ccc" };

            var result = coll.Select(x => x.Length);

            Assert.That(result, Is.EqualTo(new[] { 1, 2, 3 }));
        }

        [Test]
        public void FirstWhere()
        {
            var coll = new[] { "a", "bb", "ccc" };

            var result = coll.Where(x => x.Length >= 2);

            Assert.That(result, Is.EqualTo(new[] { "bb", "ccc" }));
        }

        [Test]
        public void FirstOrderBy()
        {
            var coll = new[] { "a", "abb", "bb" };

            var result = coll.OrderBy(x => x.Length);

            Assert.That(result, Is.EqualTo(new[] { "a", "bb", "abb" }));
        }

        [Test]
        public void Chaing()
        {
            var names = new[] { "Basia", "Jarek", "Anna", "Darek" };

            var result = names
                .Where(x => x.Last().Equals('a'))
                .OrderBy(x => x)
                .Select(x => x.Length);

            Assert.That(result, Is.EqualTo(new[] { 4, 5 }));
        }

        [Test]
        public void Subquesry()
        {
            var names = new[] { "Basia", "Ola", "Jarek", "Anna", "Darek", "Ewa" };

            var result = names.Where(x => x.Length == names
                .OrderBy(y => y.Length)
                .First().Length);

            Assert.That(result, Is.EqualTo(new[] { "Ola", "Ewa" }));
        }


        //Usuń wszystkie samogłoski z listy słów a następnie przedstaw w kolejności alfabetycznej imiona, których długość nadal wynosi więcej niż dwa znaki

        [Test]
        public void Task()
        {
            var names = new[] { "Basia", "Ola", "Jarek", "Anna", "Darek", "Ewa" };

            //var result = names.Select(x => x.Where(l => Regex.Match(l.ToString(), "[^aeiou]").Value.First()));
            var result = names.Select(x => Regex.Replace(x, "[aeiou]", "", RegexOptions.IgnoreCase))
                .Where(x => x.Length > 2)
                .OrderBy(x => x);

            //Assert.That(result, Is.EqualTo(new[] {  }));
        }


        [Test]
        public void SingleFirst()
        {
            var names = new[] { "Basia", "Ola", "Jarek", "Anna", "Darek", "Ewa" };

            var result = names.First(x => x.Length == 3);

            Assert.That(result, Is.EqualTo("Ola"));

            Assert.That(() => names.Single(x => x.Length == 3), Throws.InstanceOf<Exception>());
        }


        [Test]
        public void SingleFirstDefualt()
        {
            var names = new[] { "Basia" };//, "Ola", "Jarek", "Anna", "Darek", "Ewa" };

            var result = names.SingleOrDefault();

            Assert.That(result, Is.EqualTo("Basia"));

            Assert.That(() => names.Single(x => x.Length == 3), Throws.InstanceOf<Exception>());
        }

        [Test]
        public void Last()
        {
            var names = new[] { "Basia", "Ola", "Jarek", "Anna", "Darek", "Ewa" };

            var result = names.Last(x => x.Length == 3);

            Assert.That(result, Is.EqualTo("Ewa"));

            Assert.That(() => names.Single(x => x.Length == 3), Throws.InstanceOf<Exception>());
        }

        [Test]
        public void Skip()
        {
            var names = new[] { "Basia", "Ola", "Jarek", "Anna", "Darek", "Ewa" };

            var result = names.Skip(4);

            Assert.That(result, Is.EqualTo(new object[] { "Darek", "Ewa" }));

            Assert.That(() => names.Single(x => x.Length == 3), Throws.InstanceOf<Exception>());
        }

        [Test]
        public void SkipWhile()
        {
            var names = new[] { "Basia", "Ola", "Jarek", "Anna", "Darek", "Ewa" };

            var result = names.SkipWhile(x => x.First() != 'D');

            Assert.That(result, Is.EqualTo(new object[] { "Darek", "Ewa" }));

            Assert.That(() => names.Single(x => x.Length == 3), Throws.InstanceOf<Exception>());
        }

        [Test]
        public void Zip()
        {
            var names = new[] { "Basia", "Ola", "Jarek", "Anna", "Darek", "Ewa" };
            var surnames = new[] { "SBasia", "SOla", "SJarek", "SAnna", "SDarek", "SEwa" };

            var result = names.Zip(surnames, (x, y) => $"{x} {y}");
        }

        [Test]
        public void Ex1()
        {
            var col = Enumerable.Range(0, 10);

            var result = col.Where(x => x % 2 == 0);
        }

        [Test]
        public void Ex2()
        {
            var col = Enumerable.Range(-5, 20);

            var result = col.Where(x => x > 0 && x < 12);
        }


        [Test]
        public void Ex3()
        {
            var col = Enumerable.Range(2, 5);

            var result = col.Select(x => new { Number = x, Sqr = x * x });

            Assert.That(result.First().Sqr, Is.EqualTo(4));
        }

        [Test]
        public void Ex4()
        {
            var col = new[] { 1, 2, 3, 4, 1, 2, 3, 1, 2, 1 };

            var result = col.Distinct().Select(x => $"{x} {col.Count(y => x == y)}");
        }

        [Test]
        public void Ex5()
        {
            var col = "Some nice string which is just tested";

            var result = col.Distinct().Select(x => $"{x} {col.Count(y => x == y)}");
        }

        [Test]
        public void Ex7()
        {
            var col = new[] { 1, 2, 3, 4, 1, 2, 3, 1, 2, 1 };

            var result = col.Distinct().Select(x => $"{x} {col.Count(y => x == y) * x} {col.Count(y => x == y)}");
        }

        [Test]
        public void Ex8()
        {
            var names = new[] { "Basia", "Ola", "Jarek", "Anna", "Darek", "Ewa" };

            var result = names.Where(x => Regex.IsMatch(x, "^A.*a$"));
        }

        [Test]
        public void E11()
        {
            var names = new[] { 1, 2, 11, 13, 4, 5 };

            var result = names.OrderBy(x => -x).Take(3);
        }

        [Test]
        public void Ex12()
        {
            var s = "this IS a STRING ";
            var u = s.Split(new[] { ' ' }).Where(x => Regex.IsMatch(x, "[A-Z]"));
        }

        [Test]
        public void Ex20()
        {
            var names = new[] { "Basia", "Ola", "Jarek", "Anna", "Darek", "Ewa" };

            var result = names.Where((x, i) => i != 3);
        }

        [Test]
        public void Ex21()
        {
            var names = new[] { "Basia", "Ola", "Jarek", "Anna", "Darek", "Ewa" };
            var start = 2;
            var count = 2;
            var result = names.Where((x, i) => i < start || i >= start + count);
        }

        [Test]
        public void Ex23()
        {
            var names = new[] { "a", "b" };
            var numbers = new[] { "1", "2" };

            var result = names.Select(x => numbers.Select(y => $"{x} {y}"));
        }

        [Test]
        public void Ex24()
        {
            var names = new[] { "a", "b" };
            var numbers = new[] { "1", "2" };
            var colours = new[] { "r", "g" };

            var result = names.SelectMany(x => numbers.SelectMany(y => colours.Select(z => $"{x} {y} {z}")));
        }

        [Test]
        public void Ex25()
        {
            var names = new[] { "a", "b" };
            var numbers = new[] { "b", "c" };

        }
    }
}
