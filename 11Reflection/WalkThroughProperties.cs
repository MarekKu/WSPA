﻿using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace _11Reflection
{
    [TestFixture]
    public class WalkThroughPropertiesTests
    {
        [Test]
        public void NameOfType()
        {
            var type = typeof(Main);

            Assert.That(type.Name, Is.EqualTo("Main"));
        }

        [Test]
        public void WTGetAllProperiesOfValueType()
        {
            var result = WalThroughProperties.GetAllValueTypeAndStringProperties(new Main());

            Assert.That(result, Is.EqualTo(new[] { "MainA", "MainB", "MainC", "MainD" }));
        }

        [Test]
        public void WTGetAllPropertiesReferenceType()
        {
            var result = WalThroughProperties.GetAllClassPropertiesAndNotString(typeof(Main));

            Assert.That(result, Is.EqualTo(new[] { typeof(SubClass) }));
        }

        [Test]
        public void WalkThrough()
        {
            FakeSqlExecutor.Hits = new List<string>();
            var subObj = new SubClass();
            subObj.List = new[] { new ListElement { MyProperty = 1 } };

            var main = new Main();
            main.Sub = subObj;
            WalThroughProperties.WalkThrough(main);

            Assert.That(FakeSqlExecutor.Hits.First, Does.StartWith("M"));
        }

        [Test]
        public void Test()
        {
            var obj = new { a = "aaa", b = "bbb" };

            var val_a = GetValObjDy(new Main(), "a"); //="aaa"
            var val_b = GetValObjDy(obj, "b"); //="bbb"
        }
        //create in a static class
        static public object GetValObjDy(object obj, string propertyName)
        {
            var prop = obj.GetType().GetProperties();
            return obj.GetType().GetProperty(propertyName).GetValue(obj, null);
        }
    }

    public static class WalThroughProperties
    {
        public static void WalkThrough(object obj)
        {
            var type = obj.GetType();
            FakeSqlExecutor.Hits.Add(type.Name + ": " + string.Join(", ", WalThroughProperties.GetAllValueTypeAndStringProperties(obj)));

            foreach (var propertyInfo in WalThroughProperties.GetAllClassPropertiesAndNotString(obj))
            {
                var value = propertyInfo.GetValue(obj);
                if (value != null)
                {
                    WalThroughProperties.WalkThrough(propertyInfo.GetValue(obj));
                }
            }
            foreach (var propertyInfo in WalThroughProperties.GetAllCollectionProperties(obj))
            {
                var value = propertyInfo.GetValue(obj);
                if (value != null)
                {
                    var elements = (IEnumerable)propertyInfo.GetValue(obj);
                    foreach (var e in elements)
                    {
                        WalThroughProperties.WalkThrough(e);
                    }
                }
            }
        }


        public static IEnumerable<string> GetAllValueTypeAndStringProperties(object obj)
        {
            var props = obj.GetType().GetProperties();

            return props
                .Where(p => p.PropertyType.IsValueType || p.PropertyType == typeof(string))
                .Select(p => p.Name);
        }


        public static IEnumerable<PropertyInfo> GetAllClassPropertiesAndNotString(object obj)
        {
            var type = obj.GetType();
            var props = type.GetProperties();

            return props
                .Where(p => p.PropertyType.IsClass && p.PropertyType != typeof(string));
        }

        public static IEnumerable<PropertyInfo> GetAllCollectionProperties(object obj)
        {
            var props = obj.GetType().GetProperties();

            return props
                .Where(p => p.PropertyType.Name.StartsWith("ICollection"));
        }
    }

    #region
    public class Main
    {
        public int MainA { get; set; }
        public string MainB { get; set; }
        public decimal MainC { get; set; }
        public DateTime MainD { get; set; }

        public SubClass Sub { get; set; }
    }

    public class SubClass
    {
        public int SubClassA { get; set; }

        public SubSubClass SubSub { get; set; }

        public ICollection<ListElement> List { get; set; }
    }

    public class SubSubClass
    {
        public int SubsubClassA { get; set; }
    }

    public class FakeSqlExecutor
    {
        public static IList<string> Hits { get; set; }
    }

    public class ListElement
    {
        public int MyProperty { get; set; }
    }
    #endregion
}
