﻿using System;
using System.Linq;
using System.Reflection;
using NUnit.Framework;

namespace _11Reflection
{
    public abstract class A
    {
        public int P1 { get; set; }
    }

    public abstract class B : A
    {
        public int P2 { get; set; }
    }

    [TestFixture]
    public class UnitTest1
    {
        [Test]
        public void TestMethod1()
        {
            var result = typeof(B).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            Assert.That(result.First().Name, Is.EqualTo("P2"));
            Assert.That(result.Last().Name, Is.EqualTo("P1"));
        }
    }
}
